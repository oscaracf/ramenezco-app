<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\FrecuentClientPointsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SuppliersController;
use App\Http\Controllers\PurchasesController;
use App\Http\Controllers\MovementsController;
use App\Http\Controllers\BalancesController;
use App\Http\Controllers\NotificationsController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\NotificationsAdmiController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\ScheduleEmpController;
use App\Http\Controllers\EmpleadoController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login');
});

/* Route::get('/client', function () {
    return view('client.index');
});

Route::get('/client/create', [ClientController::class,'create']);
*/
Route::resource('client', ClientController::class);

Route::resource('employee',EmployeeController::class);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('role', RoleController::class);

Route::resource('frecuentClientPoints', FrecuentClientPointsController::class);
Route::get('/frecuentClientPoints/{frecuentClientPoint}/redeem', [App\Http\Controllers\FrecuentClientPointsController::class, 'redeem']);
Route::get('/frecuentClientPoints/{frecuentClientPoint}/list', [App\Http\Controllers\FrecuentClientPointsController::class, 'history_user']);

Route::get('/notificationsEmp', [App\Http\Controllers\NotificationsEmpController::class, 'index']);



Route::get('/evento2', [App\Http\Controllers\Evento2Controller::class, 'index']);
Route::get('/evento2/mostrar', [App\Http\Controllers\Evento2Controller::class, 'show']);
Route::post('/evento2/agregar', [App\Http\Controllers\Evento2Controller::class, 'store']);



Route::get('/evento3', [App\Http\Controllers\Evento3Controller::class, 'index']);
Route::get('/evento3/mostrar', [App\Http\Controllers\Evento3Controller::class, 'show']);
Route::post('/evento3/agregar', [App\Http\Controllers\Evento3Controller::class, 'store']);
Route::post('/evento3/editar/{id}', [App\Http\Controllers\Evento3Controller::class, 'edit']);
Route::post('/evento3/actualizar/{evento3}', [App\Http\Controllers\Evento3Controller::class, 'update']);
Route::post('/evento3/borrar/{id}', [App\Http\Controllers\Evento3Controller::class, 'destroy']);



/*Route::get('/employee',function () {return view('employee.index');});
Route::get('employee/create',[EmployeeController::class,'create']);*/

Route::resource('suppliers', App\Http\Controllers\SupplierController::class)->middleware('auth');
Route::resource('products', App\Http\Controllers\ProductController::class)->middleware('auth');
Route::resource('purchases', App\Http\Controllers\PurchaseController::class)->middleware('auth');
Route::resource('movements', App\Http\Controllers\MovementController::class)->middleware('auth');
Route::resource('balances', App\Http\Controllers\BalanceController::class)->middleware('auth');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

# agregamos vista de reportes
Route::get("list-blog", [App\Http\Controllers\DataController::class, "listBlogs"]);

Route::resource('gastos', App\Http\Controllers\GastoController::class)->middleware('auth');
Route::resource('dates', App\Http\Controllers\DateController::class)->middleware('auth');
Route::resource('requests', App\Http\Controllers\RequestController::class)->middleware('auth');

/*empleados y admi*/ 
Route::resource('notification',NotificationsController::class );
Route::resource('notificationsAdmi', NotificationsAdmiController::class);


Route::resource('schedule', ScheduleController::class);


Route::resource('scheduleEmp', ScheduleEmpController::class);




//empleado
/*Route::get('/empleado', function () {
    return view('empleado.index'); 
});

Route::get('/empleado/create', [EmpleadoController::class,'create']);
*/
Route::resource('empleado',EmpleadoController::class);

