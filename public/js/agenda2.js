document.addEventListener('DOMContentLoaded', function() {

    let formulario = document.querySelector("form");

    var calendarEl = document.getElementById('agenda2');

    var calendar = new FullCalendar.Calendar(calendarEl, {

      initialView: 'dayGridMonth',      

      locale:"es",

      displayEventTime:false,


      headerToolbar: {
          left:'prev,next today',
          center:'title',
          right:'dayGridMonth,timeGridWeek,listWeek'
      },


      events: "http://localhost/ramenezco-app/public/evento2/mostrar",



    });



    calendar.render();


    document.getElementById("btnGuardar").addEventListener("click", function(){
        const datos= new FormData(formulario);

        console.log(datos);
        console.log(formulario.id_employee.value);

        axios.post("http://localhost/ramenezco-app/public/evento2/agregar", datos).
        then(
            (respuesta)=>{
                $("#evento2").modal("hide");
            }
        ).catch(
            error=>{
                if(error.response){
                    console.log(error.response.data);
                }
            }
        )


    });


  });