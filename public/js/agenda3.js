document.addEventListener('DOMContentLoaded', function() {

    let formulario = document.querySelector("form");

    var calendarEl = document.getElementById('agenda3');

    var calendar = new FullCalendar.Calendar(calendarEl, {

      initialView: 'dayGridMonth',

      locale:"es",

      headerToolbar:{
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,listWeek'
      },


      events:"http://localhost/ramenezco-app/public/evento3/mostrar",


      dateClick:function(info){        
        formulario.reset();

          formulario.start.value=info.dateStr;
          formulario.end.value=info.dateStr;

          $("#evento3").modal("show");
    },



    eventClick:function (info){

        var evento3= info.event;
        console.log(evento3);


        axios.post("http://localhost/ramenezco-app/public/evento3/editar/"+info.event.id).
        then(
            (respuesta)=>{
                formulario.id_employee.value=respuesta.data.id_employee;
                formulario.name.value=respuesta.data.name;
                formulario.occupation.value=respuesta.data.occupation;
                formulario.start.value=respuesta.data.start;
                formulario.end.value=respuesta.data.end;
                formulario.title.value=respuesta.data.title;
                formulario.id.value=respuesta.data.id;


                $("#evento3").modal("show");
            }
            ).catch(
                error=>{
                    if(error.response){
                        console.log(error.response.data);
                    }
                }
            )


      }


    });

    calendar.render();

    document.getElementById("btnGuardar").addEventListener("click", function(){
        enviarDatos("http://localhost/ramenezco-app/public/evento3/agregar")
    });

    document.getElementById("btnEliminar").addEventListener("click",function(){
        enviarDatos("http://localhost/ramenezco-app/public/evento3/borrar/"+formulario.id.value);
    });

    document.getElementById("btnModificar").addEventListener("click",function(){
        enviarDatos("http://localhost/ramenezco-app/public/evento3/actualizar/"+formulario.id.value);
    });

   
    
    function enviarDatos(url){

        const datos= new FormData(formulario);
        axios.post(url, datos).
        then(
            (respuesta)=>{
                calendar.refetchEvents();
                $("#evento3").modal("hide");
            }
            ).catch(
                error=>{
                    if(error.response){console.log(error.response.data);}
                }
            )
    }

  });