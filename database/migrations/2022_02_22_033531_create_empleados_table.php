<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();


            $table->integer('id_employee')->unique()->index();
            $table->string('province');
            $table->string('canton');
            $table->string('district');
            $table->string('adress');
            $table->string('email');
            $table->date('date_entrance');
            $table->integer('phone_number');
            $table->string('occupation');
            $table->boolean('car');
            $table->integer('current_salary');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
