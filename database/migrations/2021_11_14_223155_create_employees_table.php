<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
          
            $table->integer('id_employee')->unique()->index();
            //$table->boolean('type_admin');
            $table->string('allergies');
            $table->string('province');
            $table->string('canton');
            $table->string('district');
            $table->string('adress');
            $table->date('date_entrance');
            $table->integer('phone_number');
            $table->string('occupation');
            $table->boolean('car');
            $table->integer('current_salary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
