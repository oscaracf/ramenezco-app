<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvento2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento2s', function (Blueprint $table) {
            $table->id();


            $table->integer('id_employee')->unique()->index();
            $table->string('name');
            $table->string('occupation');

            $table->dateTime('start');
            $table->dateTime('end');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento2s');
    }
}
