<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvento3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento3s', function (Blueprint $table) {
            $table->id();


            $table->integer('id_employee')->unique()->index();
            $table->string('name');
            $table->string('occupation');

            $table->string('start');
            $table->string('end');

            $table->string('title',255);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento3s');
    }
}
