<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('balances', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->integer('Codigo');
            $table->date('FechaMovimiento');
            $table->integer('SaldoActual');
            $table->timestamps();
      		});

        Schema::table('balances', function (Blueprint $table) {
                $table->unsignedBigInteger('product_id');
            
                $table->foreign('product_id')->references('id')->on('products');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}
