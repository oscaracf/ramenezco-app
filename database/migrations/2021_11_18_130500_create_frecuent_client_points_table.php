<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrecuentClientPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frecuent_client_points', function (Blueprint $table) {
            $table->id()->unique()->index();
            $table->integer('id_user');
            $table->integer('id_points');
            $table->integer('purchase_amount')->nullable();
            $table->integer('points_earned');
            $table->string('transaction_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frecuent_client_points');
    }
}
