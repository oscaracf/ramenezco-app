<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Codigo');
            $table->string('Descripcion');
            $table->string('Telefono');
            $table->string('Provincia');
            $table->string('Canton');
            $table->string('Distrito');
            $table->string('Ubicacion');
            $table->string('Contacto');
            $table->timestamps();
      		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
