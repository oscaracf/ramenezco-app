<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('purchases', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->string('NumeroFactura');
            $table->date('FechaCompra');
            $table->integer('cantidad');
            $table->float('valorUnitario', 8, 2);
            $table->timestamps();
      		});

        Schema::table('purchases', function (Blueprint $table) {
                $table->unsignedBigInteger('product_id');
            
                $table->foreign('product_id')->references('id')->on('products');
            });

        Schema::table('purchases', function (Blueprint $table) {
                $table->unsignedBigInteger('supplier_id');
            
                $table->foreign('supplier_id')->references('id')->on('suppliers');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
