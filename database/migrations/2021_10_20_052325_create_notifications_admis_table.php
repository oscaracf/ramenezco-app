<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsAdmisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_admis', function (Blueprint $table) {
            $table->id();

            $table->integer('id_employee')->unique()->index();
            $table->string('name');
            $table->string('first_last_name');
            $table->string('occupation');
            $table->dateTime('schedule');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_admis');
    }
}
