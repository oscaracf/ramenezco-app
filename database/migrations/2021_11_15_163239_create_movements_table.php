<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('movements', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id');
            $table->integer('Codigo');
            $table->date('FechaMovimiento');
            $table->integer('cantidad');
            $table->timestamps();
      		});

        Schema::table('movements', function (Blueprint $table) {
                $table->unsignedBigInteger('purchase_id');
            
                $table->foreign('purchase_id')->references('id')->on('purchases');
            });

        Schema::table('movements', function (Blueprint $table) {
                $table->unsignedBigInteger('product_id');
            
                $table->foreign('product_id')->references('id')->on('products');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}
