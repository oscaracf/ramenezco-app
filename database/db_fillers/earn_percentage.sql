INSERT INTO `earn_percentage` (`id`, `description`, `porcentaje`, `maxima_acumulacion`) VALUES ('1', 'Bronce', '2', '100'), ('2', 'Plata', '5', '200'), ('3','Oro','10', null);


INSERT INTO `frecuent_client_points` (`id_user`, `id_points`, `purchase_amount`, `points_earned`, `transaction_type`, `created_at`, `updated_at`) VALUES ('107120392', '1', '1000', '20', 'earn', '2021-11-18 07:09:43', NULL), ('107120392', '1', '2000', '40', 'earn', '2021-11-18 07:09:43', NULL), ('107120392', '1', '15500', '310', 'earn', '2021-11-10 07:09:43', NULL), ('107120392', '1', '18700', '374', 'earn', '2021-11-01 07:09:43', NULL);

INSERT INTO `roles` (`id`, `description`) VALUES ('1', 'Administrador'), ('2', 'Empleado'), ('3', 'Cliente');

INSERT INTO `points` (`id_points`, `description`) VALUES ('1', 'Bronce'), ('2', 'Plata'), ('3', 'Oro');

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(107120392, 'Oscar Vargas', 'o@o.com', NULL, '$2y$10$uPoWNnexwWhiFjWeRrYYke0olycHm7eCvfkgQK6P4uQ4t/MMoaEBK', 1, NULL, '2021-11-29 05:57:36', '2021-11-29 05:57:36'),
(114930759, 'Oscar Cespedes Fernandez', 'oscaracf92@gmail.com', NULL, '$2y$10$DRhBAHgzMTJuYpW2W.3TReRvcC/ETdOAOI7TYg/KRV92oslWK2m2K', 1, NULL, '2021-11-29 05:57:18', '2021-11-29 05:57:18');
