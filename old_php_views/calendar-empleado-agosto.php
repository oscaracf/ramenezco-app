<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Calendar - Calendars are used in a lot of apps. We thought to include one for React.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Calendars are used in a lot of apps. We thought to include one for React.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php'; ?>
    </div>
    <div class="app-main">
        <?php include 'side-bar-employees.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="pe-7s-date icon-gradient bg-warm-flame"> </i> </div>
                            <div>Calendario
                                <div class="page-title-subheading">Calendario mensual con su horario laboral. </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                    <li class="nav-item"> <a type="button" class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-lg"> <span>Modificar</span> </a> </li>
                </ul>
                <div class="btn-group">
                    <button type="button" class="fc-prev-button btn btn-primary" aria-label="prev" onclick="location.href='calendar-empleado-julio.php'"> <span class="fa fa-chevron-left"></span> </button>
                    <button type="button" class="fc-next-button btn btn-primary" aria-label="next" onclick="location.href='calendar-empleado-sept.php'"> <span class="fa fa-chevron-right"></span></button>
                </div>
                <br>
                <br>
                
                <!-- AGOSTO 2021 -->
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Agosto 2021</h5>
                            <table class="mb-0 table">
                                <thead>
                                    <tr>
                                        <th>Domingo</th>
                                        <th>Lunes</th>
                                        <th>Martes</th>
                                        <th>Miercoles</th>
                                        <th>Jueves</th>
                                        <th>Viernes</th>
                                        <th>Sabado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                        <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-sm">6</td>
                                        <td>7</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">8</th>
                                        <td>9</td>
                                        <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-sm">10</td>
                                        <td>11</td>
                                        <td>12</td>
                                        <td>13</td>
                                        <td>14</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">15</th>
                                        <td>16</td>
                                        <td>17</td>
                                        <td>18</td>
                                        <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-sm">19</td>
                                        <td>20</td>
                                        <td>21</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">22</th>
                                        <td>23</td>
                                        <td>24</td>
                                        <td>25</td>
                                        <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-sm">26</td>
                                        <td>27</td>
                                        <td>28</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">29</th>
                                        <td>30</td>
                                        <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target=".bd-example-modal-sm">31</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--AGOSTO 2021 --> 
                
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>

<!--MENSAJE-->
<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Turno Actual</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            <p>Daniel Fonseca Lopez</p>
                <p>Horario Laboral: 08:00 a.m. - 01:00 p.m.</p>
                <p>Ocupación en el restaurante: Mesero</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!-- MENSAJE MODIFICAR-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modificar mi Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            <div  class="row">
            <div class="col-md-4 mb-2">
            <label for="lname">Nombre:</label> <input type="text" class="form-control" id="lname" name="lname" value="Daniel" disabled><br> 
            <label for="lname">Apellido 2:</label> <input type="text" class="form-control" id="lname" name="lname" value="Fonseca" disabled><br>
            </div>
            <div class="col-md-4 mb-2">
            <label for="lname">Apellido 1:</label> <input type="text" class="form-control" id="lname" name="lname" value="Lopez" disabled><br>
            <label for="lname">Identificación:</label> <input type="text" class="form-control" id="lname" name="lname" value="11-1245-0414" disabled><br>
            </div>
            </div>
            <p>Horario Actual:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                                            <button class="btn btn-outline-primary">Turno del Empleado:</button>
                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                                            </div>
                                        </div>  
                                        <br><br>   
                <p>Nuevo Horario:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                                            <button class="btn btn-outline-primary">Nuevo Turno:</button>
                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>

                                            </div>
                                        </div>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                <!--<div id="toast-container" class="toast-top-right" id="collapseExample">
                <div class="toast toast-success" aria-live="polite">
                    <button type="button" class="close" aria-label="Close" class="toast-top-right"><span aria-hidden="true">×</span></button>
                    <div class="toast-title">Hola</div>
                    <div class="toast-message">Y adios </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
