<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Calendar - Calendars are used in a lot of apps. We thought to include one for React.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Calendars are used in a lot of apps. We thought to include one for React.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php'; ?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="pe-7s-car icon-gradient bg-warm-flame"> </i> </div>
                            <div>Calendar
                                <div class="page-title-subheading">Calendars are used in a lot of apps. We thought to include one for React. </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                    <li class="nav-item"> <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0"> <span>Basic Calendar</span> </a> </li>
                    <li class="nav-item"> <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1"> <span>List View</span> </a> </li>
                    <li class="nav-item"> <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2"> <span>Background Events</span> </a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                        <div class="main-card mb-3 card">
                            <div class="card-body">
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                        <div class="main-card mb-3 card">
                            <div class="card-body">
                                <div id='calendar-list'></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                        <div class="main-card mb-3 card">
                            <div class="card-body">
                                <div id="calendar-bg-events"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
            <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            //$('.fc-event-container').hide();
                                            var $day = $('.fc-day-top');
                                            $day.click(function (e) { 
                                                e.preventDefault();
                                                alert("Day selected: " + $(this).attr('data-date'));
                                                $( ".fc-basicDay-button" ).trigger( "click" );
                                                
                                                
                                            });

                                            /*var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        console.log("No ignorado");
                                                        $('#exampleModal').modal('show');
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');  
                                                }, false);
                                            });*/
                                        }, false);
                                    })();
                                </script> 
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
