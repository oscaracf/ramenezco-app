<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar-employees.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Notificaciones
                            <div class="page-title-subheading">Respuesta de la solicitud enviada para el cambio de horario </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-4">
                    <div class="alert alert-danger alert-dismissible fade show mb-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        Su solicitud de cambio de horario ha sido<strong> rechazada.</strong><br>
                        Su horario actual es 15/FEB/2020 con su turno de 10:00 a.m.- 06:00 p.m. </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="alert alert-success alert-dismissible fade show mb-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        Su solicitud de cambio de horario ha sido<strong> aprobada.</strong><br>
                        Su nuevo horario es 20/MAR/2020 con su turno de 11:00 a.m.- 07:00 p.m. </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="alert alert-danger alert-dismissible fade show mb-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        Su solicitud de cambio de horario ha sido<strong> rechazada.</strong><br>
                        Su horario actual es 10/ENE/2020 con su turno de 10:00 a.m.- 06:00 p.m. </div>
                </div>
            </div>
        </div>
        <div class="app-main__inner"> </div>
        <?php include 'footer-nav.php'; ?>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
