<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Acumulación Puntos de Cliente Frecuente - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>

<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Acumulación de Puntos de Cliente Frecuente
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                        <div class="page-title-actions">
                            <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark"> <i class="fa fa-star"></i> </button>
                            <div class="d-inline-block dropdown">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info"> <span class="btn-icon-wrapper pr-2 opacity-7"> <i class="fa fa-business-time fa-w-20"></i> </span> Buttons </button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <ul class="nav flex-column">
                                        <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-inbox"></i> <span> Inbox </span>
                                            <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                            </a> </li>
                                        <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-book"></i> <span> Book </span>
                                            <div class="ml-auto badge badge-pill badge-danger">5</div>
                                            </a> </li>
                                        <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-picture"></i> <span> Picture </span> </a> </li>
                                        <li class="nav-item"> <a disabled href="javascript:void(0);" class="nav-link disabled"> <i class="nav-link-icon lnr-file-empty"></i> <span> File Disabled </span> </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Acumulación Puntos de Cliente Frecuente:</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-2 mb-3">
                                    <label for="validationCustom01">Identificación:</label>
                                    <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" value="01-0000-9999" readonly>
                                    <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <label for="validationCustom01">Tipo de Cliente Frecuente:</label>
                                    <div class="dropdown" >
                                        <button id="dropdownCustom" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Plata</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button id="btnPl" type="button" tabindex="0" class="dropdown-item">Plata</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <label for="validationCustom02">Porcentaje de Acumulación:</label>
                                    <input type="text" class="form-control" id="validationCustom03" placeholder="%" value="2%" readonly>
                                    <div class="invalid-feedback" > Formato Inválido. Numero entero con dos decimales que se mostrará como porcentaje.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Monto de Compra:</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="0" value="">
                                    <div class="invalid-feedback"> Formato Inválido. Esta conformada por un número entero, de máximo siete dígitos: XXXXXXX..<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom02">Puntos a Acumular:</label>
                                    <input type="text" class="form-control" id="validationCustom02" placeholder="%" value="0" readonly>
                                    <div class="invalid-feedback" > Formato Inválido. Numero entero<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit" >Acumular Puntos</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        console.log("No ignorado");
                                                        $('#exampleModal').modal('show');
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');  
                                                }, false);
                                            });
                                            $('#validationCustom01').change(function (e) { 
                                                e.preventDefault();
                                               
                                                var aux = 0.02 * $('#validationCustom01').val();
                                                console.log(aux);
                                                $('#validationCustom02').val(aux);
                                                
                                               
                                                console.log("si entra");
                                                
                                                
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Resultado de Asignación de Puntos de Cliente Frecuente.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">XXXX han sido asignados satisfactoriamente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
