<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Modificar Cliente - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>

<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Modificar Cliente
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Modificación de Cliente:</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Usuario:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text" id="inputGroupPrepend">@</span> </div>
                                        <input type="email" class="form-control" id="validationCustomUsername" placeholder="xxxxxxxx@xxxxxx.com" value="luis@arrozbasmati.com" aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback"> Formato Inválido. Usuario debe ser en formato de Correo Electrónico: xxxxxxxx@xxxxxx.com.<br>
                                            Información Obligatoria. </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Contraseña</label>
                                    <br>
                                    <button id="generate-password" class="btn btn-primary" type="submit" >Generar y Enviar Contraseña</button>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Nombre:</label>
                                    <input type="text" class="form-control" id="validationCustom03" placeholder="Nombre" value="Luis" required>
                                    <div class="invalid-feedback"> Formato Inválido.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Apellido 1:</label>
                                    <input type="text" class="form-control" id="validationCustom04" placeholder="Apellido 1:" value="Hall" required>
                                    <div class="invalid-feedback"> Formato Inválido.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Apellido 2:</label>
                                    <input type="text" class="form-control" id="validationCustom05" placeholder="Apellido 2:" value="Chacón" required>
                                    <div class="invalid-feedback"> Formato Inválido.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Identificación:</label>
                                    <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" value="01-0000-9999" readonly>
                                    <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Tipo de Cliente Frecuente:</label>
                                    <div class="dropdown" >
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Oro</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">Oro</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button id="modify-client" class="btn btn-primary" type="submit" >Modificar Cuenta</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                $('#modify-client').click(function (e) { 
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        $('#modifyClientModal').modal('show');
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');
                                                });
                                                $('#generate-password').click(function (e) { 
                                                    event.preventDefault();
                                                    $('#generatePasswordModal').modal('show');
                                                });

                                                
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="modifyClientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modificar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cuenta ha sido modificada satisfactoriamente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="generatePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Generación de Contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">La contraseña temporal se ha generado y se ha enviado satisfactoriamente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="$('#generatePasswordModal').modal('hide');">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
