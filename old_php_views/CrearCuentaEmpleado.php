<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Registro de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>

<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Registro de Empleado
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Crear cuenta de empleado</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Nombre</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom01"  value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: está conformado por caracteres solo de tipo letra </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Primer Apellido</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: está conformado por caracteres solo de tipo letra </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Segundo Apellido</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: está conformado por caracteres solo de tipo letra </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Identificación:</label>
                                    <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" value="" required>
                                    <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom03">Alergias a algún tipo de comida o condimentos</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom03"  required>
                                        <div class="invalid-feedback"> Información obligatoria: se escribirá detalladamente en texto las alergias que el empleado tenga, si no tiene solo se escribirá “ninguna” </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Otras Señas:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: Otras Señas.</div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Provincia:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: Provincia.</div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Cantón:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: Cantón.</div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Distrito:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: Distrito.</div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Fecha de Nacimiento:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: dd/MMM/yyyy </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Correo Electrónico:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text" id="inputGroupPrepend">@</span> </div>
                                        <input type="email" class="form-control" id="validationCustomUsername" placeholder="xxxxxxxx@xxxxxx.com" aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback"> Formato Inválido. Usuario debe ser en formato de Correo Electrónico: xxxxxxxx@xxxxxx.com.<br>
                                            Información Obligatoria. </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Fecha de ingreso</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: dd/MMM/yyyy </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Teléfono:</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: XXXX-XXXX </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom03">Ocupacion dentro del restaurante:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="validationCustom03"  required>
                                        <div class="invalid-feedback"> Información obligatoria: no se permite números y el único signo de puntuación que se permite será la coma (,) si el empleado tiene más de un puesto </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom03">Automóvil</label>
                                    <div class="input-group">
                                        <div role="group" class="btn-group-sm btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-focus">
                                                <input type="radio" name="options" id="option7" autocomplete="off">
                                                Sí </label>
                                            <label class="btn btn-focus">
                                                <input type="radio" name="options" id="option8" autocomplete="off">
                                                No </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Salario:</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" id="validationCustom02" value="" required>
                                        <div class="invalid-feedback"> Información obligatoria: el salario será numerico en colones </div>
                                    </div>
                                </div>
                                <div class="col-md-8 mb-3">
                                    <label for="validationCustom02">Horario:</label>
                                    <br>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Lunes</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Martes</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Miércoles</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Jueves</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Viernes</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Sábado</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                    <div class="dropdown d-inline-block">
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Domingo</button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                            <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                            <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Contraseña:</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="Contraseña" value="Contraseña Temporal Autogenerada" disabled>
                                    <div class="invalid-feedback"> Formato Inválido. Debe contener un total de 8 caracteres, debe incluir 1 mayúscula, 1 símbolo, 1 números y 1 letra.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                            </div>
                            <button id="create-employee" class="btn btn-primary" type="submit">Crear Cuenta</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                $('#create-employee').click(function (e) { 
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        $('#modifyClientModal').modal('show');
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');
                                                });  
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Resultado de Creación de Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cuenta ha sido creada satisfactoriamente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
