<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Reporte Sugerencia de Compras de Productos
                            <div class="page-title-subheading">Productos Empleados en Ramenezco. </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Productos</h5>
                            <table class="mb-0 table">
                                <thead>
                                    <tr>
                                        <th>Codigo Producto</th>
                                        <th>Descripción</th>
                                        <th>Sugerencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">100-001</th>
                                        <td>Arroz Basmati</td>
                                        <td>Pedido antes de 4 días</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">300-007</th>
                                        <td>Te Blanco</td>
                                        <td>Pedido antes de 28 días</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">800-028</th>
                                        <td>Carne Cordero</td>
                                        <td>Pedido antes de -2 días</td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div> </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
