<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Crear un Producto Nuevo </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Introduzca la información solicitada</h5>
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Codigo de Producto</label>
                                    <input type="text" class="form-control" id="validationCustom01" placeholder="First name" value="800-029" required>
                                    <div class="valid-feedback"> Looks good! </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Descripcion</label>
                                    <input type="text" class="form-control" id="validationCustom02" placeholder="Last name" value="Azafrán en polvo clase 2" required>
                                    <div class="valid-feedback"> Looks good! </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Unidad de Medida</label>
                                    <input type="text" class="form-control" id="validationCustom02" placeholder="Last name" value="Kilogramos" required>
                                    <div class="valid-feedback"> Looks good! </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                    <label class="form-check-label" for="invalidCheck"> ¿Acepta crear este nuevo producto? </label>
                                    <div class="invalid-feedback"> You must agree before submitting. </div>
                                </div>
                            </div>
                            <button class="btn btn-success" type="submit" >Crear</button>
                            <button class="btn btn-primary" type="submit" onclick="window.location.href='ListarModificarProductos.php'">Cancelar</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    form.classList.add('was-validated');
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
