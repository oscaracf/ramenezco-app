<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Resultado de Búsqueda - Empleados - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>

<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Resultado de Búsqueda 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body" >
                            <h5 class="card-title">Empleado</h5>
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Primer Apellido</th>
                                            <th>Segundo Apellido</th>
                                            <th>Identificación</th>
                                            <th>Correo Electrónico</th>
                                            <th>Teléfono</th>
                                            <th>Ocupación</th>
                                            <th>Perfil</th>
                                            <th>Modificar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="row1">
                                            <th scope="row">1</th>
                                            <td>Luis</td>
                                            <td>Hall</td>
                                            <td>Chacón</td>
                                            <td>01-0000-9999</td>
                                            <td>luis@arrozbasmati.com</td>
                                            <td>8888-5555</td>
                                            <td>Cocinero</td>
                                            <td><button class="view-btn mb-2 mr-2 btn btn-warning" >Ver</button></td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-employee.php'" >Modificar</button></td>
                                            <td><button id="delete-button1" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr id="row2">
                                            <th scope="row">2</th>
                                            <td>Valeria</td>
                                            <td>Solis</td>
                                            <td>Chacón</td>
                                            <td>01-11111-9999</td>
                                            <td>valeria@arrozbasmati.com</td>
                                            <td>8888-5555</td>
                                            <td>Salonero</td>
                                            <td><button class="view-btn mb-2 mr-2 btn btn-warning" >Ver</button></td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-employee.php'" >Modificar</button></td>
                                            <td><button id="delete-button2" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr id="row3">
                                            <th scope="row">3</th>
                                            <td>Sandro</td>
                                            <td>Herrera</td>
                                            <td>Nieto</td>
                                            <td>01-2222-9999</td>
                                            <td>sandro@arrozbasmati.com</td>
                                            <td>8556-7788</td>
                                            <td>Bartender</td>
                                            <td><button class="view-btn mb-2 mr-2 btn btn-warning" >Ver</button></td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-employee.php'" >Modificar</button></td>
                                            <td><button id="delete-button2" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                            var row = 0;
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to

                                            $('.view-btn').click(function (e) { 
                                                e.preventDefault();
                                                $('#viewModal').modal('show');
                                            });

                                            $('.btn-danger').click(function (e) { 
                                                e.preventDefault();
                                                var button = $(this).attr('id');
                                                switch(button) {
                                                    case "delete-button1":
                                                        row = 1;
                                                    break;
                                                    case "delete-button2":
                                                        row = 2;
                                                    break;
                                                    case "delete-button3":
                                                        row = 3;
                                                    break;
                                                    case "delete-button4":
                                                        row = 4;
                                                    break;
                                                }
                                                $('#exampleModal').modal('show');
                                                console.log("si entra");
                                                
                                            });


                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        var textField = $('#validationCustom01').val();
                                                        if(textField==1){
                                                            $('#exampleModal').modal('show');
                                                        } 
                                                        else window.location.href='search-result.php'
                                                                                                
                                            
                                                       // $('#exampleModal').modal('show');
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');  
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                function removeRow (aux){
                                    console.log("si");
                                    
                                    $('#row'+aux).remove();
                                    $('#exampleModal').modal('hide');
                                    
                                }
                                function hideModal (){
                                    $('#viewModal').modal('hide');
                                    
                                }
                                </script>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <form class="needs-validation" novalidate>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom01">Nombre</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom01"  value="Luis" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Primer Apellido</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="Hall" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Segundo Apellido</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="Chacón" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom01">Identificación:</label>
                            <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" value="01-0000-9999" readonly>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom03">Alergias a algún tipo de comida o condimentos</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom03" value="Paprika"  readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Otras Señas:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="Santa Martha" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Provincia:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="San Jose" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Cantón:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="Montes de Oca" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Distrito:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="San Pedro" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Fecha de Nacimiento:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="06/12/1980" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustomUsername">Correo Electrónico:</label>
                            <div class="input-group">
                                <div class="input-group-prepend"> <span class="input-group-text" id="inputGroupPrepend">@</span> </div>
                                <input type="email" class="form-control" id="validationCustomUsername" placeholder="xxxxxxxx@xxxxxx.com" aria-describedby="inputGroupPrepend" value="luis@arrozbasmati.com" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Fecha de ingreso</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="11/04/2019" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Teléfono:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="8888-5555" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom03">Ocupacion dentro del restaurante:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom03" value="Cocinero"  readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom03">Automóvil</label>
                            <div class="input-group">
                                <div role="group" class="btn-group-sm btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-focus">
                                        <input type="radio" name="options" id="option7" autocomplete="off">
                                        Sí </label>
                                    <label class="btn btn-focus">
                                        <input type="radio" name="options" id="option8" autocomplete="off">
                                        No </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom02">Salario:</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustom02" value="650,000" readonly>
                            </div>
                        </div>
                        <div class="col-md-8 mb-3">
                            <label for="validationCustom02">Horario:</label>
                            <br>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Lunes</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Martes</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Miércoles</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Jueves</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Viernes</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Sábado</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                            <div class="dropdown d-inline-block">
                                <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Domingo</button>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                    <button type="button" tabindex="0" class="dropdown-item">07:00am/03:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">10:00am/06:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">03:00/09:00pm</button>
                                    <button type="button" tabindex="0" class="dropdown-item">Libre</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="hideModal();">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
