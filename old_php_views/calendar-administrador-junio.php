<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
        </div>              
        <div class="app-main">
            <?php include 'side-bar.php'; ?>

            <div class="app-main__outer">
                <div class="app-main__inner">


                <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-date icon-gradient bg-warm-flame">
                                        </i>
                                    </div>
                                    <div>Calendario
                                        <div class="page-title-subheading">Calendario mensual con el horario laboral de los empleados.
                                        </div>
                                    </div>
                                </div>


                                

                                <div class="page-title-actions">
                                    <div class="d-inline-block dropdown">

                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Inbox
                                                        </span>
                                                        <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">
                                                        <i class="nav-link-icon lnr-book"></i>
                                                        <span>
                                                            Book
                                                        </span>
                                                        <div class="ml-auto badge badge-pill badge-danger">5</div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="javascript:void(0);" class="nav-link">
                                                        <i class="nav-link-icon lnr-picture"></i>
                                                        <span>
                                                            Picture
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a disabled href="javascript:void(0);" class="nav-link disabled">
                                                        <i class="nav-link-icon lnr-file-empty"></i>
                                                        <span>
                                                            File Disabled
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>            
                        
                        <div class="btn-group"><button type="button" class="fc-prev-button btn btn-primary" aria-label="prev">
                            <span class="fa fa-chevron-left"></span>
                    </button><button type="button" class="fc-next-button btn btn-primary" aria-label="next" onclick="location.href='calendar-administrador-julio.php'">
                        <span class="fa fa-chevron-right"></span></button></div>
                        <br>

                        <br>

                    <!-- JULIO 2021 -->
                        <div class="col-lg-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body"><h5 class="card-title">Junio 2021</h5>
                                        <table class="mb-0 table">
                                            <thead>
                                            <tr>
                                                <th>Domingo</th>
                                                <th>Lunes</th>
                                                <th>Martes</th>
                                                <th>Miercoles</th>
                                                <th>Jueves</th>
                                                <th>Viernes</th>
                                                <th>Sabado</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th scope="row"></th>
                                                <td></td>
                                                <td>1</td>
                                                <td>2</td>
                                                <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">3</td>
                                                <td>4</td>
                                                <td>5</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>7</td>
                                                <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">8</td>
                                                <td>9</td>
                                                <td>10</td>
                                                <td>11</td>
                                                <td>12</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">13</th>
                                                <td>14</td>
                                                <td>15</td>
                                                <td>16</td>
                                                <td>17</td>
                                                <td>18</td>
                                                <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">19</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">20</th>
                                                <td>21</td>
                                                <td>22</td>
                                                <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">23</td>
                                                <td>24</td>
                                                <td>25</td>
                                                <td>26</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">27</th>
                                                <td>28</td>
                                                <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">29</td>
                                                <td>30</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- JULIO 2021 -->
                       
        </div>
        <?php include 'footer-nav.php'; ?>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>

<!--MENSAJE-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Horarios Laborales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p class="mb-0">
                    <ul>
                    <li>Daniel Fonseca Lopez - Cocinero
                        <br><strong>Horario Laboral:</strong> 01:00 p.m.- 06:00 p.m
                    </li>
                    <br>
                    <li>Maria Calvo Montero - Mesero
                        <br><strong>Horario Laboral:</strong> 10:00 a.m.- 06:00 p.m.
                    </li>
                    <br>
                    <li>Andrea Quesada Araya - Cocinero
                        <br><strong>Horario Laboral:</strong> 09:00 a.m.- 02:00 p.m.
                    </li>
                    </ul> 
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>