1- Nombre del proyecto: Sistemas Informático Ramenezco

2- Integrantes: 
- Valeria Vargas
- Luis Hall
- Oscar Céspedes

3- Descripción del proyecto:
Es un sistema que permita administrar el personal, los clientes frecuentes y los inventarios-proveedores del restaurante cuenta con los siguientes módulos:
Módulos:
MOD1-Pantalla Principal
Iniciar en la página web del restaurante. Ramenezco.site.
MOD2-Inicio de Sesión
Ingresar al sistema como cliente, empleado o administrador.
MOD3-Empleados
Crear empleados nuevos, modificar o borrar empleados existentes, Las personas empleadas pueden ver su perfil y solicitar cambios de horario de trabajo.
MOD4-Productos
Crear productos nuevos, modificar o borrar productos existentes, Buscar productos existentes.
MOD5-Proveedores
Crear proveedores nuevos, modificar o borrar proveedores existentes, Buscar proveedores existentes.
MOD6-Compras
Ingresar facturas de compra con el detalle de los productos y proveedores empleados.
MOD7-Movimientos de Productos
Ingresar movimientos al restaurante de productos utilizados en el restaurante.
MOD8-Reportes
Generar reportes que muestran tiempo promedio empleado por los proveedores para hacer entregar, gasto promedio de productos usados en el restaurante y reporte de productos en existencia con sistema de aviso.
MOD9-Calendario
Permite que el administrador pueda definir el calendario de trabajo diario de cada persona empleada.  Además las personas empleadas pueden solicitar modificaciones en su calendario de trabajo que la persona administradora aprueba o rechaza.
MOD10-Clientes
Permite que los clientes del restaurante puedan crear una cuenta en el restaurante como clientes frecuentes.
MOD11-Puntos
El restaurante puede mantener un historial de las compras de sus clientes frecuentes y mantener un sistema de puntuación por las compras realizadas. Posteriormente puede tratar a los clientes de acuerdo a distintas categorías y ofrecer descuentos de acuerdo a estas categorías.

4- Cómo instalar el repositorio en el equipo para desarrollo
Pasos
4.1- En bitbucket, visita la página principal del repositorio https://bitbucket.org/oscaracf/ramenezco-app/src/master/.
4.2- De clic en Clone.
4.3- Copiar el comando git clone https://oscaracf@bitbucket.org/oscaracf/ramenezco-app.git para clonar el repositorio mediante la línea de comando. Si desea utilizar una herramienta visual saltarse al paso 4.5.
4.4- Abrir la línea de comando, buscar el folder deseado y pegar el comando "git clone https://oscaracf@bitbucket.org/oscaracf/ramenezco-app.git" luego presionar enter.
4.5- Si desea instalar el repositorio con una herramienta puede preinstalar Sourcetree o VS Code y bitbucket le permitira integrar ambas plataformas con el repositorio.
4.6- Usted podra observar los archivos del repositorio en el folder seleccionado de su computadora de manera local.

