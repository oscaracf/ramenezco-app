<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php include 'top-nav.php';?>
    </div>
    <div class="app-main">
        <?php include 'side-bar.php'; ?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="pe-7s-refresh-2 icon-gradient bg-warm-flame"> </i> </div>
                            <div>Calendario Empleados
                                <div class="page-title-subheading">Modificar el horario laboral del empleado. </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Empleados</h5>
                        <table class="mb-0 table">
                            <thead>
                                <tr>
                                    <th>Identificación</th>
                                    <th>Nombre</th>
                                    <th>Apellido 1</th>
                                    <th>Apellido 2</th>
                                    <th>Ocupación</th>
                                    <th>Modificar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">11-1245-0414</th>
                                    <td>Daniel</td>
                                    <td>Fonseca</td>
                                    <td>Lopez</td>
                                    <td>Cocinero</td>
                                    <td><a type="button" class="mb-2 mr-2 btn btn-warning" id="tab-1" data-toggle="modal" data-target=".DanielFonsecaLopez"> <span>Modificar</span> </a></td>
                                </tr>
                                <tr>
                                    <th scope="row">20-3144-0587</th>
                                    <td>Maria</td>
                                    <td>Calvo</td>
                                    <td>Montero</td>
                                    <td>Mesero</td>
                                    <td><a type="button" class="mb-2 mr-2 btn btn-warning" id="tab-1" data-toggle="modal" data-target=".MariaCalvoMontero"> <span>Modificar</span> </a></td>
                                </tr>
                                <tr>
                                    <th scope="row">32-0652-0205</th>
                                    <td>Andrea</td>
                                    <td>Quesada</td>
                                    <td>Araya</td>
                                    <td>Cocinero</td>
                                    <td><a type="button" class="mb-2 mr-2 btn btn-warning" id="tab-1" data-toggle="modal" data-target=".AndreaQuesadaAraya"> <span>Modificar</span> </a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>

<!-- MENSAJE DANIEL-->
<div class="modal fade DanielFonsecaLopez" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modificar Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div  class="row">
            <div class="col-md-4 mb-2">
            <label for="lname">Nombre:</label> <input type="text" class="form-control" id="lname" name="lname" value="Daniel" disabled><br> 
            <label for="lname">Apellido 2:</label> <input type="text" class="form-control" id="lname" name="lname" value="Fonseca" disabled><br>
            </div>
            <div class="col-md-4 mb-2">
            <label for="lname">Apellido 1:</label> <input type="text" class="form-control" id="lname" name="lname" value="Lopez" disabled><br>
            <label for="lname">Identificación:</label> <input type="text" class="form-control" id="lname" name="lname" value="11-1245-0414" disabled><br>
            </div>
            </div>
                <p>Horario Actual:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                                            <button class="btn btn-outline-primary">Turno del empleado:</button>
                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                                            </div>
                                        </div>  
                                        <br><br>   
                <p>Horario Nuevo:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                                            <button class="btn btn-outline-primary">Nuevo Turno:</button>
                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                                                <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                                            </div>
                                        </div>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                <!--<div id="toast-container" class="toast-top-right" id="collapseExample">
                <div class="toast toast-success" aria-live="polite">
                    <button type="button" class="close" aria-label="Close" class="toast-top-right"><span aria-hidden="true">×</span></button>
                    <div class="toast-title">Hola</div>
                    <div class="toast-message">Y adios </div>
                </div>-->
            </div>
        </div>
    </div>
</div>

<!-- MENSAJE MARIA-->
<div class="modal fade MariaCalvoMontero" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modificar mi Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div  class="row">
                    <div class="col-md-4 mb-2">
                        <label for="lname">Nombre:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Maria" disabled>
                        <br>
                        <label for="lname">Apellido 1:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Calvo" disabled>
                        <br>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="lname">Apellido 2:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Montero" disabled>
                        <br>
                        <label for="lname">Identificacion:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="20-3144-0587" disabled>
                        <br>
                    </div>
                </div>
                <p>Horario Actual:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                    <button class="btn btn-outline-primary">Turno Actual:</button>
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span></button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                        <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                    </div>
                </div>
                <br>
                <br>
                <p>Dia y turno que desea:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                    <button class="btn btn-outline-primary">Turno Nuevo:</button>
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span></button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                        <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                <!--<div id="toast-container" class="toast-top-right" id="collapseExample">
                <div class="toast toast-success" aria-live="polite">
                    <button type="button" class="close" aria-label="Close" class="toast-top-right"><span aria-hidden="true">×</span></button>
                    <div class="toast-title">Hola</div>
                    <div class="toast-message">Y adios </div>
                </div>-->
            </div>
        </div>
    </div>
</div>

<!-- MENSAJE ANDREA-->
<div class="modal fade AndreaQuesadaAraya" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modificar mi Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div  class="row">
                    <div class="col-md-4 mb-2">
                        <label for="lname">Nombre:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Andrea" disabled>
                        <br>
                        <label for="lname">Apellido 1:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Quesada" disabled>
                        <br>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="lname">Apellido 2:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="Araya" disabled>
                        <br>
                        <label for="lname">Identificacion:</label>
                        <input type="text" class="form-control" id="lname" name="lname" value="32-0652-0205" disabled>
                        <br>
                    </div>
                </div>
                <br>
                <br>
                <p>Horario Actual:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                    <button class="btn btn-outline-primary">Turno Actual:</button>
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span></button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                        <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                    </div>
                </div>
                <br>
                <br>
                <p>Dia y turno que desea:</p>
                <input type="date" name="fecha">
                <div class="mb-2 mr-2 btn-group">
                    <button class="btn btn-outline-primary">Turno Nuevo:</button>
                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle-split dropdown-toggle btn btn-outline-primary"><span class="sr-only">Toggle Dropdown</span></button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                        <button type="button" tabindex="0" class="dropdown-item">08:00 a.m. 12:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">12:00 p.m. 04:00 p.m.</button>
                        <button type="button" tabindex="0" class="dropdown-item">04:00 p.m. 08:00 p.m.</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                <!--<div id="toast-container" class="toast-top-right" id="collapseExample">
                <div class="toast toast-success" aria-live="polite">
                    <button type="button" class="close" aria-label="Close" class="toast-top-right"><span aria-hidden="true">×</span></button>
                    <div class="toast-title">Hola</div>
                    <div class="toast-message">Y adios </div>
                </div>-->
            </div>
        </div>
    </div>
</div>