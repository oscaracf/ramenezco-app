<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
<?php include 'side-bar.php'; ?>
<div class="app-main__outer">
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                <div>Notificaciones
                    <div class="page-title-subheading">Acceso a las solicitudes de cambio de los empleado </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-6 col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body"  id="eliminarcarta1">
                    <h5 class="card-title">Daniel Fonseca Lopez</h5>
                    <h6 class="card-subtitle">11-1245-0414</h6>
                    Horario Actual del empleado: 06/ENE/2021.<br>
                    Turno Actual: 10:00 a.m.- 05:00 p.m<br>
                    Nuevo Horario: 10/ENE/2020.<br>
                    Nuevo Turno: 10:00 a.m.- 06:00 p.m.<br>
                    Ocupacion: Cocinero<br>
                    <br>
                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta1">Aprobar</button>
                    <button class="mb-2 mr-2 btn btn-danger" data-dismiss="alert" data-target="#eliminarcarta1">Rechazar</button>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body"  id="eliminarcarta2">
                    <h5 class="card-title">Maria Calvo Montero</h5>
                    <h6 class="card-subtitle">20-3144-0587</h6>
                    Horario Actual del empleado: 12/FEB/2021.<br>
                    Turno Actual: 11:00 a.m.- 06:00 p.m<br>
                    Nuevo Horario: 20/FEB/2021.<br>
                    Nuevo Turno: 01:00 p.m.- 06:00 p.m.<br>
                    Ocupacion: Mesero<br>
                    <br>
                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta2">Aprobar</button>
                    <button class="mb-2 mr-2 btn btn-danger" data-dismiss="alert" data-target="#eliminarcarta2">Rechazar</button>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body"  id="eliminarcarta3">
                    <h5 class="card-title">Federico Gomez Salazar</h5>
                    <h6 class="card-subtitle">1132-0652-02051202359</h6>
                    Horario Actual del empleado: 09/JUN/2021.<br>
                    Turno Actual: 08:00 a.m.- 02:00 p.m<br>
                    Nuevo Horario: 20/FEB/2021.<br>
                    Nuevo Turno: 01:00 p.m.- 06:00 p.m.<br>
                    Ocupacion: Mesero<br>
                    <br>
                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta3">Aprobar</button>
                    <button class="mb-2 mr-2 btn btn-danger" data-dismiss="alert" data-target="#eliminarcarta3">Rechazar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-6 col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body"  id="eliminarcarta4">
                    <h5 class="card-title">Andrea Quesada Araya</h5>
                    <h6 class="card-subtitle">326520206695</h6>
                    Horario Actual del empleado: 02/MAR/2021.<br>
                    Turno Actual: 09:00 a.m.- 02:00 p.m<br>
                    Nuevo Horario: 03/MAR/2001.<br>
                    Nuevo Turno: 09:00 a.m.- 02:00 p.m.<br>
                    Ocupacion: Cocinero<br>
                    <br>
                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta4">Aprobar</button>
                    <button class="mb-2 mr-2 btn btn-danger" data-dismiss="alert" data-target="#eliminarcarta4">Rechazar</button>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4">
            <div class="main-card mb-3 card">
                <div class="card-body"  id="eliminarcarta5">
                    <h5 class="card-title">Mario Elizondo Torres</h5>
                    <h6 class="card-subtitle">22203652047</h6>
                    Horario Actual del empleado: 20/ABR/2021.<br>
                    Turno Actual: 10:00 a.m.- 05:00 p.m<br>
                    Nuevo Horario: 10/ENE/2020.<br>
                    Nuevo Turno: 10:00 a.m.- 06:00 p.m.<br>
                    Ocupacion: Cocinero<br>
                    <br>
                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta5">Aprobar</button>
                    <button class="mb-2 mr-2 btn btn-danger" data-dismiss="alert" data-target="#eliminarcarta5">Rechazar</button>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer-nav.php'; ?>
    <div class="app-main__inner"> </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
