<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-date icon-gradient bg-warm-flame"> </i> </div>
                        <div>Calendario
                            <div class="page-title-subheading">Calendario mensual con el horario laboral de los empleados. </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-group">
                <button type="button" class="fc-prev-button btn btn-primary" aria-label="prev" onclick="location.href='calendar-administrador-julio.php'"> <span class="fa fa-chevron-left"></span> </button>
                <button type="button" class="fc-next-button btn btn-primary" aria-label="next" onclick="location.href='calendar-administrador-sept.php'"> <span class="fa fa-chevron-right"></span></button>
            </div>
            <br>
            <br>
            
            <!-- AGOSTO 2021 -->
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Agosto 2021</h5>
                        <table class="mb-0 table">
                            <thead>
                                <tr>
                                    <th>Domingo</th>
                                    <th>Lunes</th>
                                    <th>Martes</th>
                                    <th>Miercoles</th>
                                    <th>Jueves</th>
                                    <th>Viernes</th>
                                    <th>Sabado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>2</td>
                                    <td>3</td>
                                    <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">4</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>7</td>
                                </tr>
                                <tr>
                                    <th scope="row">8</th>
                                    <td>9</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">12</td>
                                    <td>13</td>
                                    <td>14</td>
                                </tr>
                                <tr>
                                    <th scope="row">15</th>
                                    <td>16</td>
                                    <td>17</td>
                                    <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">18</td>
                                    <td>19</td>
                                    <td>20</td>
                                    <td>21</td>
                                </tr>
                                <tr>
                                    <th scope="row">22</th>
                                    <td>23</td>
                                    <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">24</td>
                                    <td>25</td>
                                    <td>26</td>
                                    <td>27</td>
                                    <td>28</td>
                                </tr>
                                <tr>
                                    <th scope="row">29</th>
                                    <td class="nav-link btn" id="tab-1" data-toggle="modal" data-target="#exampleModal">30</td>
                                    <td>31</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--AGOSTO 2021 --> 
            
        </div>
        <?php include 'footer-nav.php'; ?>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>

<!--MENSAJE-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Horarios Laborales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p class="mb-0">

                    <ul>
                    <li>Daniel Fonseca Lopez - Cocinero
                        <br><strong>Horario Laboral:</strong> 01:00 p.m.- 06:00 p.m
                    </li>
                    <br>
                    <li>Maria Calvo Montero - Mesero
                        <br><strong>Horario Laboral:</strong> 10:00 a.m.- 06:00 p.m.
                    </li>
                    <br>
                    <li>Andrea Quesada Araya - Cocinero
                        <br><strong>Horario Laboral:</strong> 09:00 a.m.- 02:00 p.m.
                    </li>
                    </ul> 
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>