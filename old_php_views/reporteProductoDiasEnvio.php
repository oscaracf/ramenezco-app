<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Form Validation - Inline validation is very easy to implement using the Architect Framework.</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Reporte de días de entrega de Productos
                            <div class="page-title-subheading">Productos Empleados en Ramenezco. </div>
                        </div>
                    </div>
                    <div class="page-title-actions">
                        <div class="d-inline-block dropdown">
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                <ul class="nav flex-column">
                                    <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-inbox"></i> <span> Inbox </span>
                                        <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                        </a> </li>
                                    <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-book"></i> <span> Book </span>
                                        <div class="ml-auto badge badge-pill badge-danger">5</div>
                                        </a> </li>
                                    <li class="nav-item"> <a href="javascript:void(0);" class="nav-link"> <i class="nav-link-icon lnr-picture"></i> <span> Picture </span> </a> </li>
                                    <li class="nav-item"> <a disabled href="javascript:void(0);" class="nav-link disabled"> <i class="nav-link-icon lnr-file-empty"></i> <span> File Disabled </span> </a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Productos</h5>
                            <table class="mb-0 table">
                                <thead>
                                    <tr>
                                        <th>Codigo Producto</th>
                                        <th>Descripcion</th>
                                        <th>Proveedor</th>
                                        <th>Días de Entrega Promedio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">100-001</th>
                                        <td>Arroz Basmati</td>
                                        <td>Mas x Menos</td>
                                        <td>11</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">300-007</th>
                                        <td>Te Blanco</td>
                                        <td>Productos K</td>
                                        <td>4</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">800-028</th>
                                        <td>Carne Cordero</td>
                                        <td>Hipermas</td>
                                        <td>12</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div> </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
