
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Listar Clientes - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
</head>
<body>


<?php if(Auth::guest()): ?>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php echo $__env->make('nav.top-nav-guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<div class="app-main">

    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Clientes 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            
            <div>
            <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
</div>
            <!--<div class="row">
                <div class="col-lg-12">
                    <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>-->
            <?php echo $__env->make('nav.footer-nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>

</body>
</html>


<?php else: ?>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php echo $__env->make('nav.top-nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<div class="app-main">
<?php echo $__env->make('nav.side-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Clientes 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Clientes</h5>
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Identificación</th>
                                            <th>Nombre</th>
                                            <th>Apellido 1</th>
                                            <th>Apellido 2</th>
                                            <th>Usuario</th>
                                            <th>Cliente Frecuente</th>
                                            <th>Modificar</th>
                                            <th>Eliminar</th>
                                            <th>Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo e($aux = 1); ?>

                                        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <form action="<?php echo e(url('/client/'. $client->id_client)); ?>" method="post">
                                                <tr id="">
                                                    <td scope="row">
                                                        <?php echo e($aux++); ?>

                                                    </td>
                                                    <td><?php echo e($client->id_client); ?></td>
                                                    <td><?php echo e($client->name); ?></td>
                                                    <td><?php echo e($client->first_last_name); ?></td>
                                                    <td><?php echo e($client->second_last_name); ?></td>
                                                    <td><?php echo e($client->email); ?></td>
                                                    <td>
                                                    <?php if( $client->id_points == 1): ?>
                                                        Oro
                                                    <?php elseif($client->id_points == 2): ?>
                                                        Bronze
                                                    <?php else: ?>
                                                        Plata
                                                    <?php endif; ?>                                                         
                                                    </td>
                                                    <td><a href="<?php echo e(url('/client/'.$client->id_client.'/edit')); ?>">Editar</a></td>
                                                    <td>
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo e(method_field('DELETE')); ?>

                                                        <input type="submit" onclick="return confirm('Quieres Borrar?')" value="Borrar">
                                                    </td>
                                                    <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Acumular Puntos</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Redimir Puntos</button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>--> 
                                                    </div>
                                                </div></td>
                                                </tr>
                                            </form>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
            <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
</div>
            <!--<div class="row">
                <div class="col-lg-12">
                    <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>-->
            <?php echo $__env->make('nav.footer-nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    /*var row = 0;
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            $('.btn-danger').click(function (e) { 
                e.preventDefault();
                var button = $(this).attr('id');
                switch(button) {
                    case "delete-button1":
                        row = 1;
                    break;
                    case "delete-button2":
                        row = 2;
                    break;
                    case "delete-button3":
                        row = 3;
                    break;
                    case "delete-button4":
                        row = 4;
                    break;
                }
                $('#exampleModal').modal('show');
                console.log("si entra");
                
            });
            $('.dropdown-item').click(function (e) { 
                //$('.dropdown-menu').show();
                e.preventDefault();
                var btnAux = $(this).attr('id');
                switch(btnAux) {
                    case "btnAcu":
                        window.location.href='accumulate-points.php'
                    break;
                    case "btnRed":
                        window.location.href='redeem-points.php'
                    break;
                }
                
                console.log("si entra");
                
            });
        }, false);
    })();
    function removeRow (aux){
        console.log("si");
        
        $('#row'+aux).remove();
        $('#exampleModal').modal('hide');
        
    }*/
</script> 
<script type="text/javascript" src="<?php echo e(asset('js/main.js')); ?>"></script>
</body>
</html>
<?php endif; ?>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/ramenezco2.0/resources/views//client/index.blade.php ENDPATH**/ ?>