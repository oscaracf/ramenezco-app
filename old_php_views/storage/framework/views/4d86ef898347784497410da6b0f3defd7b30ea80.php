
<div class="app-header header-shadow">
<div class="app-header__logo">
    <div class="logo-src"></div>
    <div class="header__pane ml-auto">
        <div>
            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
        </div>
    </div>
</div>
<div class="app-header__mobile-menu">
    <div>
        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
    </div>
</div>
<div class="app-header__menu"> <span>
    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"> <span class="btn-icon-wrapper"> <i class="fa fa-ellipsis-v fa-w-6"></i> </span> </button>
    </span> </div>
<div class="app-header__content">
    <div class="app-header-left">
        <div class="search-wrapper">
            <div class="input-holder">
                <input type="text" class="search-input" placeholder="Type to search">
                <button class="search-icon"><span></span></button>
            </div>
            <button class="close"></button>
        </div>
        <ul class="header-menu nav">
            <li class="btn-group nav-item"> <a href="calendar-administrador-julio.php" class="nav-link"> <i class="nav-link-icon fa fa-edit"></i> Calendario </a> </li>
        </ul>
    </div>
    <div class="app-header-right">
        <div class="header-btn-lg pr-0">
            <div class="widget-content p-0">
                <div class="widget-content-wrapper">
                <?php if(auth()->guard()->guest()): ?>
                    <div class="widget-content-left  ml-3 header-user-info">
                        
                        
                    
                            <?php if(Route::has('login')): ?>

                                <div class="widget-heading"> <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a> </div> 
                            
                            <?php endif; ?>

                            <?php if(Route::has('register')): ?>
                                
                                <div class="widget-subheading"> <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a> </div>  
                            
                            <?php endif; ?>
                            </div>
                        <?php else: ?>
                    <div class="widget-content-left">
                        <div class="btn-group"> <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn"> <i class="pe-7s-users" style="font-size: 1.85rem;"> </i> 
                            <!--<img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">--> 
                            <i class="fa fa-angle-down ml-2 opacity-8"></i> </a>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logout')); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                                        <?php echo csrf_field(); ?>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content-left  ml-3 header-user-info">
                        <div class="widget-heading"> <?php echo e(Auth::user()->name); ?> </div>
                        <div class="widget-subheading"> Administrador </div>
                    </div>
                    <div class="widget-content-right header-user-info ml-3">
                        <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example"> <i class="fa text-white fa-calendar pr-1 pl-1"></i> </button>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/ramenezco2.0/resources/views/nav/top-nav.blade.php ENDPATH**/ ?>