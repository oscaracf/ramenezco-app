<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Creación de Cliente - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php echo $__env->make('nav.top-nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<div class="app-main">
<?php echo $__env->make('nav.side-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                        <div>Creación de Cliente
                            <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Creación de Cliente:</h5>
                    <form action="<?php echo e(url('/client' )); ?>" method="post" class="needs-validation" novalidate>
                    <?php echo csrf_field(); ?>
                    

                    <?php echo e(isset($client)?$client:""); ?>



               
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Email:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend"> <span class="input-group-text" id="inputGroupPrepend">@</span> </div>
                                    <input type="email" class="form-control" id="validationCustomUsername" placeholder="xxxxxxxx@xxxxxx.com" aria-describedby="inputGroupPrepend" name="Email" value="<?php echo e(isset($client[0]->email)?$client[0]->email:''); ?>" required>
                                    <div class="invalid-feedback"> Formato Inválido. Usuario debe ser en formato de Correo Electrónico: xxxxxxxx@xxxxxx.com.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Contraseña:</label>
                                <input type="text" class="form-control" id="validationCustom01" placeholder="Contraseña" value="Contraseña Temporal Autogenerada" disabled>
                                <div class="invalid-feedback"> Formato Inválido. Debe contener un total de 8 caracteres, debe incluir 1 mayúscula, 1 símbolo, 1 números y 1 letra.<br>
                                    Información Obligatoria. </div>
                                <div class="valid-feedback"> Formato Verificado. </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Nombre:</label>
                                <input type="text" class="form-control" id="validationCustom03" placeholder="Nombre" name="Name" value="<?php echo e(isset($client[0]->name)?$client[0]->name:''); ?>"  required>
                                <div class="invalid-feedback"> Formato Inválido.<br>
                                    Información Obligatoria. </div>
                                <div class="valid-feedback"> Formato Verificado. </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Apellido 1:</label>
                                <input type="text" class="form-control" id="validationCustom04" placeholder="Apellido 1:" name="First_Last_Name" value="<?php echo e(isset($client[0]->first_last_name)?$client[0]->first_last_name:''); ?>" required>
                                <div class="invalid-feedback"> Formato Inválido.<br>
                                    Información Obligatoria. </div>
                                <div class="valid-feedback"> Formato Verificado. </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Apellido 2:</label>
                                <input type="text" class="form-control" id="validationCustom05" placeholder="Apellido 2:" name="Second_Last_Name" value="<?php echo e(isset($client[0]->second_last_name)?$client[0]->second_last_name:''); ?>" required>
                                <div class="invalid-feedback"> Formato Inválido.<br>
                                    Información Obligatoria. </div>
                                <div class="valid-feedback"> Formato Verificado. </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Identificación:</label>
                                <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" name="Id_Client" value="<?php echo e(isset($client[0]->id_client)?$client[0]->id_client:''); ?>" required>
                                <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                    Información Obligatoria. </div>
                                <div class="valid-feedback"> Formato Verificado. </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Tipo de Cliente Frecuente:</label>
                                <div class="dropdown" >
                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">Plata</button>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                <label class="form-check-label" for="invalidCheck"> Aceptar Términos y Condiciones </label>
                                <div class="invalid-feedback"> Debe aceptar los términos y condiciones para poder continuar. </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit" >Crear Cuenta</button>
                        
                    </form>
                    <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    /*if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        console.log("No ignorado");
                                                        $('#exampleModal').modal('show');
                                                    } */
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');  
                                                }, false);
                                            });
                                            
                                        }, false);
                                    })();
                                </script> 
                </div>
            </div>
        </div>
        <?php echo $__env->make('nav.footer-nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Resultado de Creación de Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cuenta ha sido creada satisfactoriamente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo e(asset('js/main.js')); ?>"></script>
</body>
</html>






<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/ramenezco2.0/resources/views/client/create.blade.php ENDPATH**/ ?>