<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Listar Clientes - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<?php include 'scripts.php';?>

<!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
<link href="./main.css" rel="stylesheet">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php include 'top-nav.php';?>
</div>
<div class="app-main">
    <?php include 'side-bar.php'; ?>
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Clientes 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Clientes</h5>
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Usuario</th>
                                            <th>Nombre</th>
                                            <th>Apellido 1</th>
                                            <th>Apellido 2</th>
                                            <th>Identificación</th>
                                            <th>Cliente Frecuente</th>
                                            <th>Modificar</th>
                                            <th>Eliminar</th>
                                            <th>Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="row1">
                                            <th scope="row">1</th>
                                            <td>luis@arrozbasmati.com</td>
                                            <td>Luis</td>
                                            <td>Hall</td>
                                            <td>Chacón</td>
                                            <td>01-0000-9999</td>
                                            <td>Oro</td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-client.php'" >Modificar</button></td>
                                            <td><button id="delete-button1" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                            <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Acumular Puntos</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Redimir Puntos</button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>--> 
                                                    </div>
                                                </div></td>
                                        </tr>
                                        <tr id="row2">
                                            <th scope="row">2</th>
                                            <td>valeria@arroztiopelon.com</td>
                                            <td>Valeria</td>
                                            <td>Vargas</td>
                                            <td>Vargas</td>
                                            <td>01-1111-2222</td>
                                            <td>Plata</td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-client.php'" >Modificar</button></td>
                                            <td><button id="delete-button2" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                            <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Acumular Puntos</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Redimir Puntos</button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>--> 
                                                    </div>
                                                </div></td>
                                        </tr>
                                        <tr id="row3">
                                            <th scope="row">3</th>
                                            <td>fabian@carneslaunion.com</td>
                                            <td>Fabian</td>
                                            <td>Fernandez</td>
                                            <td>Chacón</td>
                                            <td>01-4444-7799</td>
                                            <td>Bronce</td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-client.php'" >Modificar</button></td>
                                            <td><button id="delete-button3" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                            <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Acumular Puntos</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Redimir Puntos</button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>--> 
                                                    </div>
                                                </div></td>
                                        </tr>
                                        <tr id="row4">
                                            <th scope="row">4</th>
                                            <td>oscar@prueba.com</td>
                                            <td>Oscar</td>
                                            <td>Cespedes</td>
                                            <td>Fernandez</td>
                                            <td>01-6669-7775</td>
                                            <td>Oro</td>
                                            <td><button class="mb-2 mr-2 btn btn-info"  onclick="window.location.href='modify-client.php'" >Modificar</button></td>
                                            <td><button id="delete-button4" class="mb-2 mr-2 btn btn-danger">Eliminar</button></td>
                                            <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Acumular Puntos</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Redimir Puntos</button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>--> 
                                                    </div>
                                                </div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php include 'footer-nav.php'; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    var row = 0;
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            $('.btn-danger').click(function (e) { 
                e.preventDefault();
                var button = $(this).attr('id');
                switch(button) {
                    case "delete-button1":
                        row = 1;
                    break;
                    case "delete-button2":
                        row = 2;
                    break;
                    case "delete-button3":
                        row = 3;
                    break;
                    case "delete-button4":
                        row = 4;
                    break;
                }
                $('#exampleModal').modal('show');
                console.log("si entra");
                
            });
            $('.dropdown-item').click(function (e) { 
                //$('.dropdown-menu').show();
                e.preventDefault();
                var btnAux = $(this).attr('id');
                switch(btnAux) {
                    case "btnAcu":
                        window.location.href='accumulate-points.php'
                    break;
                    case "btnRed":
                        window.location.href='redeem-points.php'
                    break;
                }
                
                console.log("si entra");
                
            });
        }, false);
    })();
    function removeRow (aux){
        console.log("si");
        
        $('#row'+aux).remove();
        $('#exampleModal').modal('hide');
        
    }
</script> 
<script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>
</html>
