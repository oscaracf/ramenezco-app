<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
        </div>
    </div>
    <div class="app-header__menu"> <span>
        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"> <span class="btn-icon-wrapper"> <i class="fa fa-ellipsis-v fa-w-6"></i> </span> </button>
        </span> </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">General</li>
                <li> <a href="notificaciones-administrador.php" class=""> <i class="metismenu-icon pe-7s-rocket"></i> Notificaciones </a> </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond" class="mm-active"></i> Calendario <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="modificar-calendarioemp.php"> <i class="metismenu-icon"> </i>Modificar Calendario </a> </li>
                        <li> <a href="calendar-administrador-julio.php" > <i class="metismenu-icon"> </i>Mostrar Calendario </a> </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading">Administrar</li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Productos <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="ListarModificarProductos.php"> <i class="metismenu-icon"> </i>Listar y Modificar Productos </a> </li>
                        <li> <a href="crearProductos.php"> <i class="metismenu-icon"></i> Crear Productos </a> </li>
                        <li> <a href="buscarProductos.php"> <i class="metismenu-icon"> </i>Buscar y Modificar Productos </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Proveedores <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="listarModificarProveedores.php"> <i class="metismenu-icon"> </i>Listar y Modificar Proveedores </a> </li>
                        <li> <a href="crearProveedores.php"> <i class="metismenu-icon"></i> Crear Proveedores </a> </li>
                        <li> <a href="buscarProveedores.php"> <i class="metismenu-icon"> </i>Buscar y Modificar Proveedores </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Facturas <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="Facturas.php"> <i class="metismenu-icon"> </i>Ingresar Facturas </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Movimientos de Productos <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="movimientosProductos.php"> <i class="metismenu-icon"> </i>Ingresar Movimientos </a> </li>
                        <li> <a href="actualizarSaldosProductos.php"> <i class="metismenu-icon"></i> Actualizar Saldos de Productos </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Empleados <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="CrearCuentaEmpleado.php"> <i class="metismenu-icon"> </i>Crear Empleado </a> </li>
                        <li> <a href="CuentasEmpleados.php"> <i class="metismenu-icon"></i> Buscar Empleado </a> </li>
                        <li> <a href="CuentasEmpleados.php"> <i class="metismenu-icon"></i> Actualizar Empleado </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Clientes <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="list-clients.php"> <i class="metismenu-icon"> </i>Listar Clientes </a> </li>
                        <li> <a href="search-client.php"> <i class="metismenu-icon"></i> Buscar Cliente </a> </li>
                        <li> <a href="create-client.php"> <i class="metismenu-icon"> </i>Agregar Cliente </a> </li>
                        <li> <a href="search-client.php"> <i class="metismenu-icon"> </i>Modificar Cliente </a> </li>
                        <li> <a href="search-client.php"> <i class="metismenu-icon"> </i>Eliminar Cliente </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Puntos Cliente Frecuente <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="define-percentage.php"> <i class="metismenu-icon"> </i>Porcentaje de Acumulación </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Reportes <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="reporteProductoDiasEnvio.php"> <i class="metismenu-icon"> </i>Días Promedio de Entrega </a> </li>
                        <li> <a href="reporteProductoGasto.php"> <i class="metismenu-icon"></i> Gasto Promedio Semanal </a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
