<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Producto') }}
            {{ Form::text('Producto', $date->Producto, ['class' => 'form-control' . ($errors->has('Producto') ? ' is-invalid' : ''), 'placeholder' => 'Producto']) }}
            {!! $errors->first('Producto', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Dias Promedio de Entrega') }}
            {{ Form::text('DiasEntrega', $date->DiasEntrega, ['class' => 'form-control' . ($errors->has('DiasEntrega') ? ' is-invalid' : ''), 'placeholder' => 'Dias Promedio de Entrega']) }}
            {!! $errors->first('DiasEntrega', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
</div>