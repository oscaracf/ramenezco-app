@extends('layouts.app')

@section('template_title')
    {{ $supplier->name ?? 'Show Supplier' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Mostrar Proveedores</span>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Codigo:</strong>
                            {{ $supplier->Codigo }}
                        </div>
                        <div class="form-group">
                            <strong>Descripcion:</strong>
                            {{ $supplier->Descripcion }}
                        </div>
                        <div class="form-group">
                            <strong>Telefono:</strong>
                            {{ $supplier->Telefono }}
                        </div>
                        <div class="form-group">
                            <strong>Provincia:</strong>
                            {{ $supplier->Provincia }}
                        </div>
                        <div class="form-group">
                            <strong>Canton:</strong>
                            {{ $supplier->Canton }}
                        </div>
                        <div class="form-group">
                            <strong>Distrito:</strong>
                            {{ $supplier->Distrito }}
                        </div>
                        <div class="form-group">
                            <strong>Ubicacion:</strong>
                            {{ $supplier->Ubicacion }}
                        </div>
                        <div class="form-group">
                            <strong>Contacto:</strong>
                            {{ $supplier->Contacto }}
                        </div>
                        <div class="float-left">
                            <a class="btn btn-primary" href="{{ route('suppliers.index') }}"> Volver</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
