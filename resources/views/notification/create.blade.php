<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Enviar Mensaje - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">



<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">





    //<script src="{{ asset('js/app.js') }}" defer></script>


    </head>
<body>




//
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Mensajes
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
//





<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">

    @include('nav.top-nav')
</div>

<div class="app-main">
@include('nav.side-bar-employee')


    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Mensajes
                            <div class="page-title-subheading">Crear mensaje para enviar al Administrador </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                                <div class="container-fluid">
                                    @if(session('message'))
                                    <div class="row mb-2">
                                        <div class="alert alert-success">
                                            {{ session('message') }}
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            <form action="{{ route('notification.store') }}" method="POST">
                                @csrf
                                    <div class="form-group">
                                        <label>Cédula</label>
                                            <input type="text" class="form-control" name="id_employee">
                                    </div>
                                    <div class="form-group">
                                        <label>Título</label>
                                            <input type="text" class="form-control" name="title">
                                    </div>
                                    <div class="form-group">
                                        <label>Descripción</label>
                                            <input type="text" class="form-control" name="description">
                                    </div>
                                    <button class="btn btn-success" type="submit">Enviar</button>
                            </form>
            </div>

                             </div>
                           @include('nav.footer-nav')
 
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>



@endif
