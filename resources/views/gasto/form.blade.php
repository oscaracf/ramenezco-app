<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Producto') }}
            {{ Form::text('Producto', $gasto->Producto, ['class' => 'form-control' . ($errors->has('Producto') ? ' is-invalid' : ''), 'placeholder' => 'Producto']) }}
            {!! $errors->first('Producto', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Gasto por Producto Semanal') }}
            {{ Form::text('GastoProducto', $gasto->GastoProducto, ['class' => 'form-control' . ($errors->has('GastoProducto') ? ' is-invalid' : ''), 'placeholder' => 'GastoProducto']) }}
            {!! $errors->first('GastoProducto', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="float-left">
        <a class="btn btn-success" href="{{ route('gastos.index') }}"> Volver</a>
    </div>
</div>