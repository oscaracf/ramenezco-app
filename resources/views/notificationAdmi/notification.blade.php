<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Lista de Mensajes - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">


    /*<script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">*/


    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body>

//
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                <div>Notificaciones
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
//






<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">

    @include('nav.top-nav')
</div>

<div class="app-main">
@include('nav.side-bar')

<div class="app-main__outer">
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                <div>Notificaciones
                    <div class="page-title-subheading">Acceso a las solicitudes de cambio de los empleado </div>
                </div>
            </div>
        </div>
    </div>
 <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Mensajes</h5>
                         <a href="{{ url('/schedule/') }}" class="btn btn-success"> Modificar horarios </a>
                            <br>
                            <br>
                            <table class="table table-light">
                                <thead class="thead-light">
                                    <tr>
                                        <th># </th>
                                        <th>Cédula </th>
                                        <th>Título </th>
                                        <th>Descripción </th>
                                        <th>Acciones </th>
                                    </tr>
                                </thead>
                            <span style="display:none"> {{ $aux = 1; }}</span>
                            <tbody>
                        @foreach($notifications as $notifications)                     
                            <tr>
                                    <td>{{ $aux++ }}</td>
                                    <td>{{ $notifications->id_employee }}</td>
                                    <td>{{ $notifications->title }}</td>
                                    <td>{{ $notifications->description }}</td>
                                    <td>
                        <form action="{{ url('/notificationsAdmi/'.$notifications->id ) }}"  method="post">
                                    @csrf
                                    {{ method_field('DELETE')}}
                            <input class="btn btn-danger" type="submit" onclick="return confirm('Desea borrar?')"
                                value="Borrar">
                        </form>
             </td>
        </tr>
@endforeach
    </tbody>
</table>
            </div>

    </div>

</div>

           @include('nav.footer-nav')

    </div>
   </div>
    </div>
    
</div>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

</body>
</html>





//
@endif