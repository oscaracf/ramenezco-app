<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Numero Factura') }}
            {{ Form::text('NumeroFactura', $purchase->NumeroFactura, ['class' => 'form-control' . ($errors->has('NumeroFactura') ? ' is-invalid' : ''), 'placeholder' => 'Numero factura']) }}
            {!! $errors->first('NumeroFactura', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Fecha Factura') }}
            {{ Form::text('FechaCompra', $purchase->FechaCompra, ['class' => 'form-control' . ($errors->has('FechaCompra') ? ' is-invalid' : ''), 'placeholder' => 'Fecha Factura']) }}
            {!! $errors->first('FechaCompra', '<div class="invalid-feedback">:message</p>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('Cantidad') }}
            {{ Form::text('cantidad', $purchase->cantidad, ['class' => 'form-control' . ($errors->has('cantidad') ? ' is-invalid' : ''), 'placeholder' => 'Cantidad']) }}
            {!! $errors->first('cantidad', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Valor por unidad') }}
            {{ Form::text('valorUnitario', $purchase->valorUnitario, ['class' => 'form-control' . ($errors->has('valorUnitario') ? ' is-invalid' : ''), 'placeholder' => 'Valor unitario']) }}
            {!! $errors->first('valorUnitario', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Producto Id') }}
            {{ Form::text('product_id', $purchase->product_id, ['class' => 'form-control' . ($errors->has('product_id') ? ' is-invalid' : ''), 'placeholder' => 'Producto Id']) }}
            {!! $errors->first('product_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Proveedor Id') }}
            {{ Form::text('supplier_id', $purchase->supplier_id, ['class' => 'form-control' . ($errors->has('supplier_id') ? ' is-invalid' : ''), 'placeholder' => 'Proveedor Id']) }}
            {!! $errors->first('supplier_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
    <div class="float-left">
        <a class="btn btn-success" href="{{ route('purchases.index') }}"> Volver</a>
    </div>

</div>