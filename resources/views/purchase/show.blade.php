@extends('layouts.app')

@section('template_title')
    {{ $purchase->name ?? 'Show Purchase' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Mostrar Factura</span>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Numero factura:</strong>
                            {{ $purchase->NumeroFactura }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha Factura:</strong>
                            {{ $purchase->FechaCompra }}
                        </div>
                        <div class="form-group">
                            <strong>Cantidad:</strong>
                            {{ $purchase->cantidad }}
                        </div>
                        <div class="form-group">
                            <strong>Valor por unidad:</strong>
                            {{ $purchase->valorUnitario }}
                        </div>
                        <div class="form-group">
                            <strong>Producto Id:</strong>
                            {{ $purchase->product_id }}
                        </div>
                        <div class="form-group">
                            <strong>Proveedor Id:</strong>
                            {{ $purchase->supplier_id }}
                        </div>
                        <div class="float-left">
                            <a class="btn btn-primary" href="{{ route('purchases.index') }}"> Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
