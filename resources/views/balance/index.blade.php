@extends('layouts.app2')

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Listar Inventario de Productos - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Inventario de Productos 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">@include('nav.top-nav-index')</div>
<div class="app-main">@include('nav.side-bar-index')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"><i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i></div>
                        <div>Listar Inventario de Productos
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Inventario de Productos</h5>
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <div class="float-left">
                                    <a href="{{ route('balances.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                      {{ __('Crear Nuevo Inventario de Producto') }}
                                    </a>
                                  </div>  
        
                                <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Codigo</th>
										<th>Fecha Inventario</th>
										<th>Inventario actual</th>
										<th>Producto Id</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($balances as $balance)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $balance->Codigo }}</td>
											<td>{{ $balance->FechaMovimiento }}</td>
											<td>{{ $balance->SaldoActual }}</td>
											<td>{{ $balance->product_id }}</td>

                                            <td>
                                                <form action="{{ route('balances.destroy',$balance->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('balances.show',$balance->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('balances.edit',$balance->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
   
                        </div>
                    </div> 
                    </div>
   
                </div>
            </div>
            
            
            @include('nav.footer-nav')</div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
@endif
