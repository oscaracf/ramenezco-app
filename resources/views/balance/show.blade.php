@extends('layouts.app')

@section('template_title')
    {{ $balance->name ?? 'Show Balance' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Mostrar Inventario de Producto</span>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Codigo:</strong>
                            {{ $balance->Codigo }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha Inventario:</strong>
                            {{ $balance->FechaMovimiento }}
                        </div>
                        <div class="form-group">
                            <strong>Inventario actual:</strong>
                            {{ $balance->SaldoActual }}
                        </div>
                        <div class="form-group">
                            <strong>Producto Id:</strong>
                            {{ $balance->product_id }}
                        </div>
                        <div class="float-left">
                            <a class="btn btn-primary" href="{{ route('balances.index') }}"> Volver</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
