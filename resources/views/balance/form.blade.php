<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Codigo') }}
            {{ Form::text('Codigo', $balance->Codigo, ['class' => 'form-control' . ($errors->has('Codigo') ? ' is-invalid' : ''), 'placeholder' => 'Codigo']) }}
            {!! $errors->first('Codigo', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Fecha de Inventario') }}
            {{ Form::text('FechaMovimiento', $balance->FechaMovimiento, ['class' => 'form-control' . ($errors->has('FechaMovimiento') ? ' is-invalid' : ''), 'placeholder' => 'Fecha de Inventario']) }}
            {!! $errors->first('FechaMovimiento', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Inventario Actual') }}
            {{ Form::text('SaldoActual', $balance->SaldoActual, ['class' => 'form-control' . ($errors->has('SaldoActual') ? ' is-invalid' : ''), 'placeholder' => 'Inventario actual']) }}
            {!! $errors->first('SaldoActual', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Producto Id') }}
            {{ Form::text('product_id', $balance->product_id, ['class' => 'form-control' . ($errors->has('product_id') ? ' is-invalid' : ''), 'placeholder' => 'Producto Id']) }}
            {!! $errors->first('product_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
    <div class="float-left">
        <a class="btn btn-success" href="{{ route('balances.index') }}"> Volver</a>
    </div>
</div>