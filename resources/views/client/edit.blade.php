
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Modificar Cliente - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Clientes 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">@include('nav.top-nav-index')</div>
<div class="app-main">@include('nav.side-bar-index')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Modificar Cliente
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Modificación de Cliente:</h5>
                        <form action="{{ url('/client/'.$client[0]->id_client) }}" method="post">
                        @csrf

                        {{method_field('PATCH')}}
                           
                            
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Identificación:</label>
                                    <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" name="Id_Client" value="{{  isset($client[0]->id_client)?$client[0]->id_client:'' }}" readonly>
                                    <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Tipo de Cliente Frecuente:</label>
                                    <div class="dropdown" >
                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-primary">@if ( $client[0]->id_points == 1)
                                                        Bronce
                                                    @elseif ($client[0]->id_points == 2)
                                                        Plata
                                                    @else
                                                    Oro 
                                                    @endif </button>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <input type="text" name="Id_Points" value="{{  isset($client[0]->id_points)?$client[0]->id_points:'' }}" >
                            <button id="modify-client" class="btn btn-primary" type="submit" >Modificar Cuenta</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                $('#modify-client').click(function (e) { 
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        /*event.preventDefault();
                                                        $('#modifyClientModal').modal('show');*/
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');
                                                });
                                                $('#generate-password').click(function (e) { 
                                                    event.preventDefault();
                                                    $('#generatePasswordModal').modal('show');
                                                });

                                                
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
                       
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                $('#modify-client').click(function (e) { 
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        /*event.preventDefault();
                                                        $('#modifyClientModal').modal('show');*/
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');
                                                });
                                                $('#generate-password').click(function (e) { 
                                                    event.preventDefault();
                                                    $('#generatePasswordModal').modal('show');
                                                });

                                                
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            @include('nav.footer-nav')
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
@endif




