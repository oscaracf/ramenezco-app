<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Listar Clientes - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Clientes 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                @include('validation.login')
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">@include('nav.top-nav-index')</div>
<div class="app-main">@include('nav.side-bar-index')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"><i class="pe-7s-drawer icon-gradient bg-happy-itmeo"></i></div>
                        <div>Listar Clientes
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Clientes</h5>
                            @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{ Session::get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Identificación</th>
                                            <th>Cliente Frecuente</th>
                                            <th>Modificar</th>
                                            <th>Eliminar</th>
                                            <th>Puntos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <span style="display:none">{{ $aux = 1; }}</span>@foreach( $clients as $client )
                                    <form action="{{ url('/client/'. $client->id_client) }}" method="post">
                                        <tr id="" style="height:90px">
                                            <td scope="row">{{ $aux++ }}</td>
                                            <td>{{ $client->id_client }}</td>
                                            <td>@if ( $client->id_points == 1)
                                                Bronce
                                                @elseif ($client->id_points == 2)
                                                Plata
                                                @else
                                                Oro
                                                @endif</td>
                                            <td><a href="{{ url('/client/'.$client->id_client.'/edit') }}">Editar</a></td>
                                            <td>@csrf
                                                {{ method_field('DELETE') }}
                                                <input type="submit" onclick="return confirm('Quieres Borrar?')" value="Borrar"></td>
                                            <td><div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">
                                                        <a href="{{ url('/frecuentClientPoints/'.$client->id_client.'/list') }}">Consultar Puntos</a>
                                                        </button>
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">
                                                        <a href="{{ url('/frecuentClientPoints/'.$client->id_client.'/edit') }}">Acumular Puntos</a>
                                                        </button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">
                                                        <a href="{{ url('/frecuentClientPoints/'.$client->id_client.'/redeem') }}">Redimir Puntos</a>
                                                        </button>
                                                        <!--<button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>-->
                                                    </div>
                                                </div></td>
                                        </tr>
                                    </form>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('nav.footer-nav')</div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
@endif