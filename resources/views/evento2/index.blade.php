@extends('layouts.app')
@section('content')
    
<div class="container">
        <div id="agenda2"> 
        
<br>




 <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#evento2">
          Solicitud para un cambio de horario laboral
        </button>

        </div>
</div>


<!-- Modal -->
<div class="modal fade" id="evento2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Solicitud para un cambio de horario laboral</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        



        <form action="">

                {!! csrf_field() !!}

    
        <div class="form-group">
            <label for="id_employee">Cédula</label>
            <input type="text" class="form-control" name="id_employee" id="id_employee" aria-describedby="helpId">
        </div>


        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId">
        </div>


       <div class="form-group">
            <label for="occupation">Ocupación</label>
            <input type="text" class="form-control" name="occupation" id="occupation" aria-describedby="helpId">
        </div>


        <div class="form-group">
            <label for="start">Fecha Actual</label>
            <input type="date" class="form-control" name="start" id="start" aria-describedby="helpId">
        </div>


        <div class="form-group">
            <label for="end">Fecha Nueva</label>
            <input type="date" class="form-control" name="end" id="end" aria-describedby="helpId" >
        </div>


        </form>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="btnGuardar">Enviar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

      </div>

 
    </div>
  </div>




</div>


@stop

