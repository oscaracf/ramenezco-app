@extends('layouts.app')

@section('template_title')
    {{ $movement->name ?? 'Show Movement' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Mostrar Entrada de Producto</span>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Codigo:</strong>
                            {{ $movement->Codigo }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha de Entrada:</strong>
                            {{ $movement->FechaMovimiento }}
                        </div>
                        <div class="form-group">
                            <strong>Cantidad:</strong>
                            {{ $movement->cantidad }}
                        </div>
                        <div class="form-group">
                            <strong>Factura Id:</strong>
                            {{ $movement->purchase_id }}
                        </div>
                        <div class="form-group">
                            <strong>Producto Id:</strong>
                            {{ $movement->product_id }}
                        </div>
                        <div class="float-left">
                            <a class="btn btn-primary" href="{{ route('movements.index') }}"> Volver</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
