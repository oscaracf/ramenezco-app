<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            {{ Form::label('Codigo') }}
            {{ Form::text('Codigo', $movement->Codigo, ['class' => 'form-control' . ($errors->has('Codigo') ? ' is-invalid' : ''), 'placeholder' => 'Codigo']) }}
            {!! $errors->first('Codigo', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Fecha de Entrada') }}
            {{ Form::text('FechaMovimiento', $movement->FechaMovimiento, ['class' => 'form-control' . ($errors->has('FechaMovimiento') ? ' is-invalid' : ''), 'placeholder' => 'Fecha de Entrada']) }}
            {!! $errors->first('FechaMovimiento', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('cantidad') }}
            {{ Form::text('cantidad', $movement->cantidad, ['class' => 'form-control' . ($errors->has('cantidad') ? ' is-invalid' : ''), 'placeholder' => 'Cantidad']) }}
            {!! $errors->first('cantidad', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Factura Id') }}
            {{ Form::text('purchase_id', $movement->purchase_id, ['class' => 'form-control' . ($errors->has('purchase_id') ? ' is-invalid' : ''), 'placeholder' => 'Factura Id']) }}
            {!! $errors->first('purchase_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Producto Id') }}
            {{ Form::text('product_id', $movement->product_id, ['class' => 'form-control' . ($errors->has('product_id') ? ' is-invalid' : ''), 'placeholder' => 'Producto Id']) }}
            {!! $errors->first('product_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Enviar    </button>
    </div>
    <div class="float-left">
        <a class="btn btn-success" href="{{ route('movements.index') }}"> Volver</a>
    </div>

</div>