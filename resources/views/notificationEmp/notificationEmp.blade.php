<!doctype html>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">


    </head>
<body>


<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">

    @include('nav.top-nav')
</div>
<div class="app-main">
@include('nav.side-bar-employee')


    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Notificaciones
                            <div class="page-title-subheading">Respuesta de la solicitud enviadas para el cambio de horario </div>
                        </div>
                    </div>
                </div>
            </div>

                        <div class="container">

                            <form action="{{ route('notification.store') }}" method="POST">
                                @csrf
                                    <div class="form-group">
                                        <label>Titulo</label>
                                            <input type="text" class="form-control" name="title">
                                    </div>
                                    <div class="form-group">
                                        <label>Descripcion</label>
                                            <input type="text" class="form-control" name="description">
                                    </div>

                
                                    <button class="btn btn-success" type="submit">Enviar</button>

                            </form>
                        </div>
           
            

    </div>

           @include('nav.footer-nav')


        </div>

 <main class="py-4">
            @yield('content')
        </main>


</body>
</html>