

<!doctype html>


@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Notificaciones
                            <div class="page-title-subheading">Respuesta de la solicitud enviada para el cambio de horario </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>


    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">


 </head>
<body>
</html>
@else


<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">

    @include('nav.top-nav')
</div>
<div class="app-main">
@include('nav.side-bar-employee')


    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Notificaciones
                            <div class="page-title-subheading">Respuesta de la solicitud enviada para el cambio de horario </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">

@foreach($notificationEmp as $notificationEmp)

           <div class="row">
                <div class="col-6 col-md-4">
                    <div class="main-card mb-3 card">
                        <div class="card-body" id="eliminarcarta2">

                
                    <h5 class="card-title">Cedula: {{ $notificationEmp-> id_employee}} </h5>
                    <h5 class="card-title">Respuesta: {{ $notificationEmp-> response}} </h5>


                    <button class="mb-2 mr-2 btn btn-success" data-dismiss="alert" data-target="#eliminarcarta2">Aceptar</button>

                </div>

                
            </div>

            
        </div>
               
    </div>



 @endforeach
                  @include('nav.footer-nav')

            </div>

 <main class="py-4">
            @yield('content')
        </main>

    </div>

</body>
</html>
@endif
