
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Modificar Usuario - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
@include('nav.top-nav')
    </div>
    <div class="app-main">
    @include('nav.side-bar-index')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Modificar Usuario
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Modificación de Cliente:</h5>
                        <form action="{{ url('/role/'.$user[0]->id) }}" method="post">
@csrf

{{method_field('PATCH')}}
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Correo Electrónico:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"> <span class="input-group-text" id="inputGroupPrepend">@</span> </div>
                                        <input type="email" class="form-control" id="validationCustomUsername" placeholder="xxxxxxxx@xxxxxx.com" name="Email" value="{{  isset($user[0]->email)?$user[0]->email:'' }}"  aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback"> Formato Inválido. Usuario debe ser en formato de Correo Electrónico: xxxxxxxx@xxxxxx.com.<br>
                                            Información Obligatoria. </div>
                                        <div class="valid-feedback"> Formato Verificado. </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Nombre:</label>
                                    <input type="text" class="form-control" id="validationCustom03" placeholder="Nombre" name="Name" value="{{  isset($user[0]->name)?$user[0]->name:'' }}" required>
                                    <div class="invalid-feedback"> Formato Inválido.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Tipo de Usuario:</label>
                                    <div class="form-group">
                                    
                                        <select class="form-control" id="Role" name="Role" value="{{  isset($user[0]->role)?$user[0]->role:'' }}">
                                            <option value="1" @if (old('role') == 1) selected @endif>Administrador</option>
                                            <option value="2" @if (old('role') == 2) selected @endif>Colaborador</option>
                                            <option value="3" @if (old('role') == 3) selected @endif>Cliente</option>
                                        </select>
                                    <div class="invalid-feedback"> Formato Inválido.<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                  <!--  <div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle btn btn-light"  id="dropdown-role">Seleccione</button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                        <button id="btnAcu" type="button" tabindex="0" class="dropdown-item">Administrador</button>
                                                        <button id="btnRed" type="button" tabindex="0" class="dropdown-item">Empleado</button>
                                                        <button type="button" tabindex="0" class="dropdown-item">Consultar Puntos</button>
                                                    </div>
                                                </div>-->
                                </div>
                            </div>
                            <!--<div class="form-row">
                                
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Identificación:</label>
                                    <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" name="Id" value="{{  isset($user[0]->id)?$user[0]->id:'' }}" readonly>
                                    <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                        Información Obligatoria. </div>
                                    <div class="valid-feedback"> Formato Verificado. </div>
                                </div>
                                
                            </div>-->
                            
                            
                            <button id="modify-client" class="btn btn-primary" type="submit" >Modificar Usuario</button>
                        </form>
                        <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            $("#dropdown-role div button").click(function(){
                                                $(".btn:first-child").html($(this).text()+' <span class="caret"></span>');
                                            });
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                $('#modify-client').click(function (e) { 
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    if (form.checkValidity() === true) {
                                                        /*event.preventDefault();
                                                        $('#modifyClientModal').modal('show');*/
                                                    } 
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');
                                                });
                                                $('#generate-password').click(function (e) { 
                                                    event.preventDefault();
                                                    $('#generatePasswordModal').modal('show');
                                                });

                                                
                                            });
                                        }, false);
                                    })();
                                </script> 
                    </div>
                </div>
            </div>
            @include('nav.footer-nav')
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>







</form>