<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Horario Laboral - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">



<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">





    //<script src="{{ asset('js/app.js') }}" defer></script>


    </head>
<body>


<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">

    @include('nav.top-nav')
</div>

<div class="app-main">
@include('nav.side-bar-employee')


    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-comment icon-gradient bg-warm-flame"> </i> </div>
                        <div>Horario Laboral
                            <div class="page-title-subheading">Se muestra el horario laboral </div>
                        </div>
                    </div>
                </div>
            </div>

<div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Horario Laboral</h5>

                        @if(Session::has('mensaje'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                        {{ Session::get('mensaje') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        @endif


<div class="form-row">
   <div class="col-xl-5">
    <form action="{{route('scheduleEmp.index')}}" method="get">
        <div class="form-row">
            <div class="col-sm-4">
                <input type="text" class="form-control" name="texto" value="{{$texto}}">
            </div>
            <div class="col-auto">
                <input type="submit" class="btn btn-primary" value="Buscar">
            </div>
        </div>
    </form>
   </div>
</div>
</br>

<table class="table table-light">

    <thead class="thead-light">
        <tr>
            <th>Numero</th>
            <th>Apellido </th>
            <th>Fecha </th>
            <th>Hora de Entrada </th>
            <th>Hora de Salida </th>
            <th>Horas Extra </th>
        </tr>
    </thead>

   <tbody>

    @foreach( $schedules as $schedules )
        <tr>
            <td>{{ $schedules->id }}</td>
            <td>{{ $schedules->id_employee }}</td>
            <td>{{ $schedules->date }}</td>
            <td>{{ $schedules->entrance_time }}</td>
            <td>{{ $schedules->departure_time }}</td>
            <td>{{ $schedules->extra_hours }}</td>
            <td>
            
        
             </td>
        </tr>
@endforeach
    </tbody>
</table>
                             </div>
                           @include('nav.footer-nav')
 
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>



