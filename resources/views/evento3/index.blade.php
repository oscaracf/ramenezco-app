@extends('layouts.app')
@section('content')
    
    <div class="container">
        <div id="agenda3">
        </div>    
    </div>


 
 

<!-- Modal -->
<div class="modal fade" id="evento3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Horario para Empleado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="">
        
        {!! csrf_field() !!}

        <div class="form-group">
            <label for="id_employee">Cédula</label>
            <input type="text" class="form-control" name="id_employee" id="id_employee" aria-describedby="helpId" placeHolder="Cédula" >
        </div>

        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeHolder="Nombre" >
        </div>


        <div class="form-group">
            <label for="occupation">Ocupación</label>
            <input type="text" class="form-control" name="occupation" id="occupation" aria-describedby="helpId" placeHolder="Ocupación" >
        </div>

        <div class="form-group">
            <label for="start">Fecha Incio</label>
            <input type="date" class="form-control" name="start" id="start" aria-describedby="helpId">
        </div>

        <div class="form-group">
            <label for="end">Fecha Fin</label>
            <input type="date" class="form-control" name="end" id="end" aria-describedby="helpId">
        </div>

          <div class="form-group">
            <label for="title">Titulo</label>
            <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" placeHolder="Titulo del evento" >
        </div>

        <div class="form-group d-none">
            <label for="id">Codigo</label>
            <input type="text" class="form-control" name="id" id="id" aria-describedby="helpId" placeHolder="Escribe el titulo" >
        </div>
        
        </form>
    


      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-success" id="btnGuardar">Guardar</button>
        <button type="button" class="btn btn-warning" id="btnModificar">Modificar</button>
        <button type="button" class="btn btn-danger"  id="btnEliminar">Eliminar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

      
      </div>
    </div>
  </div>

</div>






@stop