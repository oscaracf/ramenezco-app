<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Registro de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">

<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
    </div>
    <div class="app-main">
@include('nav.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Crear Empleado
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">




<form action="{{ url('/empleado') }}" method="post">
@csrf




<h1>Crear empleado</h1>


@if(count($errors)>0)
    
    <div class="alert alert-danger" role="alert">
<ul>
     @foreach( $errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
   
@endif



<div class="form-group">
<label for="id_employee">Cédula </label>
<input class="form-control" type="text" name="id_employee" value="{{  isset($user[0]->id)?$user[0]->id:'' }}" id="id_employee">
    <br>
</div>


<div class="form-group">
<label for="province">Provincia </label>
<input class="form-control" type="text" name="province" value="{{ isset($empleado->province)?$empleado->province:old('province')}}" id="province">
<br>
</div>



<div class="form-group">
<label for="canton">Cantón </label>
<input class="form-control" type="text" name="canton" value="{{ isset($empleado->canton)?$empleado->canton:old('canton') }}" id="canton">
<br>
</div>



<div class="form-group">
<label for="district">Distrito </label>
<input class="form-control" type="text" name="district" value="{{ isset($empleado->district)?$empleado->district:old('district') }}" id="district">
<br>
</div>


<div class="form-group">
<label for="adress">Dirección </label>
<input class="form-control" type="text" name="adress" value="{{ isset($empleado->adress)?$empleado->adress:old('adress') }}" id="adress">
<br>
</div>


<div class="form-group">
<label for="email">Correo Electrónico </label>
<input class="form-control" type="text" name="email" value="{{ isset($empleado->email)?$empleado->email:old('email') }}" id="email">
<br>
</div>



<div class="form-group">
<label for="date_entrance">Día de entrada </label>
<input class="form-control" type="date" name="date_entrance" value="{{ isset($empleado->date_entrance)?$empleado->date_entrance:old('date_entrance') }}" id="date_entrance">
<br>
</div>



<div class="form-group">
<label for="phone_number">Número telefónico </label>
<input class="form-control" type="text" name="phone_number" value="{{ isset($empleado->phone_number)?$empleado->phone_number:old('phone_number') }}" id="phone_number">
<br>
</div>



<div class="form-group">
<label for="occupation">Ocupación </label>
<input class="form-control" type="text" name="occupation" value="{{ isset($empleado->occupation)?$empleado->occupation:old('occupation') }}" id="occupation">
<br>
</div>



<div class="form-group">
<label for="car">Carro </label>
 <select class="form-control" id="car" name="car" value="{{ isset($empleado->car)?$empleado->car:'' }}">
    <option value="1" @if (old('car') == 1) selected @endif>Si</option>
    <option value="0" @if (old('car') == 0) selected @endif>No</option>
  </select>
<br>
</div>



<div class="form-group">
<label for="current_salary">Salario </label>
<input class="form-control" type="text" name="current_salary" value="{{ isset($empleado->current_salary)?$empleado->current_salary:old('current_salary') }}" id="current_salary">
<br>
</div>


                        <button class="btn btn-primary" type="submit" >Crear Empleado</button>



<a class="btn btn-primary" href="{{ url('empleado/') }}"> Regresar </a>


<br>


</form>





              </div>
                </div>
            </div>
           @include('nav.footer-nav')
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>


