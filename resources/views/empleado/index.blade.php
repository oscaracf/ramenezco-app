<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Lista de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">

<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body>


<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
    </div>

    <div class="app-main">
@include('nav.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Lista de Empleados
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Empleados</h5>







@if(Session::has('mensaje'))
<div class="alert alert-success alert-dismissible" role="alert">
{{ Session::get('mensaje') }}
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
@endif


                        <a class="btn btn-primary" href="{{ url('schedule/') }}">Horarios Laborales </a>

<br>
<br>

<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th># </th>
            <th>Cédula </th>
            <th>Provincia </th>
            <th>Cantón </th>
            <th>Distrito </th>
            <th>Dirección </th>
            <th>Correo</th>
            <th>Día de entrada </th>
            <th>Teléfono</th>
            <th>Ocupación </th>
            <th>Carro </th>
            <th>Salario </th>
            <th>Acciones </th>

        </tr>
    </thead>


    <tbody>

    @foreach( $empleados as $empleado )
        <tr>
            <td>{{ $empleado->id }}</td>
            <td>{{ $empleado->id_employee }}</td>
            <td>{{ $empleado->province }}</td>
            <td>{{ $empleado->canton }}</td>
            <td>{{ $empleado->district }}</td>
            <td>{{ $empleado->adress }}</td>
            <td>{{ $empleado->email }}</td>
            <td>{{ $empleado->date_entrance }}</td>
            <td>{{ $empleado->phone_number }}</td>
            <td>{{ $empleado->occupation }}</td>
            <td>{{ $empleado->car }}</td>
            <td>{{ $empleado->current_salary }}</td>
            <td>
            
            <a href="{{ url('/empleado/'.$empleado->id.'/edit') }}" class="btn btn-warning">
                Editar
            </a>
            
            </br>
                        </br>

            <form action="{{ url('/empleado/'.$empleado->id ) }}" class="d-inline" method="post">
            @csrf
            {{ method_field('DELETE') }}
            <input class="btn btn-danger" type="submit" onclick="return confirm('Desea borrar?')"
            value="Borrar">

            </form>
            
             </td>

        </tr>

@endforeach

    </tbody>


</table>

{!! $empleados->links() !!}

</div>

                         
                </div>
            </div>
           @include('nav.footer-nav')
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>