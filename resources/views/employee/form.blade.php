<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Registro de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">

<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
    </div>
    <div class="app-main">
@include('nav.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Registro de Empleado
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">

                        <h5 class="card-title">Crear cuenta de empleado</h5>
                        @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{ Session::get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                                @if(count($errors)>0)
    
                                        <div class="alert alert-danger" role="alert">
                                            <ul>
                                                @foreach( $errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                @endif

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                        <label for="id_employee">Cédula </label>
                                        <input class="form-control" type="text" name="id_employee" value="{{  isset($user[0]->id)?$user[0]->id:'' }}" id="id_employee" readonly>
                                                <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                    <label for="allergies">Alergias </label>
                                    <input class="form-control" type="text" name="allergies" value="{{ isset($employee[0]->allergies)?$employee[0]->allergies:old('allergies') }}" id="allergies">
                                                <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                  <div class="form-group">
                                  <label for="occupation">Ocupación </label>
                                  <input class="form-control" type="text" name="occupation" value="{{ isset($employee->occupation)?$employee->occupation:old('occupation') }}" id="occupation">
                                            <br>
                                    </div>
                                </div>
                           
                            </div>

                            <div class="form-row">


                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                    <label for="province">Provincia </label>
                                    <input class="form-control" type="text" name="province" value="{{ isset($employee->province)?$employee->province:old('province')}}" id="province">
                                                <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                   <div class="form-group">
                                    <label for="canton">Cantón </label>
                                    <input class="form-control" type="text" name="canton" value="{{ isset($employee->canton)?$employee->canton:old('canton') }}" id="canton">
                                                <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                   <div class="form-group">
                                   <label for="district">Distrito </label>
                                   <input class="form-control" type="text" name="district" value="{{ isset($employee->district)?$employee->district:old('district') }}" id="district">
                                                <br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">


                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                    <label for="adress">Dirección </label>
                                    <input class="form-control" type="text" name="adress" value="{{ isset($employee->adress)?$employee->adress:old('adress') }}" id="adress">
                                            <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                    <label for="date_entrance">Día de entrada </label>
                                    <input class="form-control" type="date" name="date_entrance" value="{{ isset($employee->date_entrance)?$employee->date_entrance:old('date_entrance') }}" id="date_entrance">
                                            <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                   <div class="form-group">
                                   <label for="phone_number">Número telefónico </label>
                                   <input class="form-control" type="text" name="phone_number" value="{{ isset($employee->phone_number)?$employee->phone_number:old('phone_number') }}" id="phone_number">
                                            <br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-group">
                                    <label for="car">Carro </label>
                                        <select class="form-control" id="car" name="car" value="{{ isset($employee->car)?$employee->car:'' }}">
                                            <option value="1" @if (old('car') == 1) selected @endif>Si</option>
                                            <option value="0" @if (old('car') == 0) selected @endif>No</option>
                                        </select>
                                            <br>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                   <div class="form-group">
                                   <label for="current_salary">Salario </label>
                                   <input class="form-control" type="text" name="current_salary" value="{{ isset($employee->current_salary)?$employee->current_salary:old('current_salary') }}" id="current_salary">
                                        <br>
                                    </div>
                                </div>

                            </div>

                                                       

                        <input class="btn btn-success" type="submit" value="{{ $modo }} datos" href="{{ url('employee/') }}">
                        
                        <a class="btn btn-primary" href="{{ url('employee/') }}"> Ver la lista de empleados </a>


                    </div>
                </div>
            </div>
           @include('nav.footer-nav')
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>


/*<script type="text/javascript">
    function generar(){
        $.ajax({
            url: 'employee/contaAleatoria.php',
            type: 'post',
                success: function (response) {
                   $("#password").val(response) ; 
                }
        });
    }
</script>*/

