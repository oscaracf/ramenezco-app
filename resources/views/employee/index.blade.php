<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Lista de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">

<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body>


<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
    </div>

    <div class="app-main">
@include('nav.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Lista de Empleados
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Empleados</h5>
                        @if(Session::has('mensaje'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{ Session::get('mensaje') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <a class="btn btn-primary" href="{{ url('schedule/') }}">Horarios Laborales </a>


                       <!-- <a href="{{ url('employee/create') }}"  class="btn btn-success"> Registrar un nuevo empleado </a> -->
                            <br>
                            <br>

                            <table class="table table-light">
                                <thead class="thead-light">
                                    <tr>
                                        <th># </th>
                                        <th>Cédula </th>
                                        <th>Alergias </th>
                                        <th>Provincia </th>
                                        <th>Cantón </th>
                                        <th>Distrito </th>
                                        <th>Dirección </th>
                                        <th>Día de entrada </th>
                                        <th>Número telefónico </th>
                                        <th>Ocupación </th>
                                        <th>Carro </th>
                                        <th>Salario </th>
                                  
                                        <th>Acciones </th>

                                    </tr>
                                </thead>

                            <span style="display:none"> {{ $aux = 1; }}</span>
                            <tbody>

                            @foreach( $employees as $employees )
                                <tr>
                                    <td>{{ $aux++ }}</td>
                                    <td>{{ $employees->id_employee }}</td>
                                    <td>{{ $employees->allergies }}</td>
                                    <td>{{ $employees->province }}</td>
                                    <td>{{ $employees->canton }}</td>
                                    <td>{{ $employees->district }}</td>
                                    <td>{{ $employees->adress }}</td>
                                    <td>{{ $employees->date_entrance }}</td>
                                    <td>{{ $employees->phone_number }}</td>
                                    <td>{{ $employees->occupation }}</td>
                                    <td>{{ $employees->car }}</td>
                                    <td>{{ $employees->current_salary }}</td>
                                  
                                    <td>
            
                        <a href="{{ url('/employee/'.$employees->id_employee.'/edit') }}" class="btn btn-warning">
                            Editar
                        </a>

                                <br> 
                                <br> 

                        <form action="{{ url('/employee/'.$employees->id_employee ) }}"  method="post">
                                    @csrf
                                    {{ method_field('DELETE') }}
                            <input class="btn btn-danger" type="submit" onclick="return confirm('Desea borrar?')"
                                value="Borrar">
                        </form>

            
             </td>

        </tr>

@endforeach

    </tbody>


</table>


</div>  
                         
                </div>
            </div>
           @include('nav.footer-nav')
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>

