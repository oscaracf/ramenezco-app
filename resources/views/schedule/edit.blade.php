<form action="{{ url('/schedule/'.$schedule[0]->id_employee )}}" method="post">
@csrf
{{ method_field('PATCH')}}

@include('schedule.form',['modo'=>'Editar']);

</form>
