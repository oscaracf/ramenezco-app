<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Lista de Empleado - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">



<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">

</head>
<body>


//
@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"> @include('nav.top-nav-guest') </div>
<div class="app-main">
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Lista de Empleados
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Buen Intento!</h4>
                    <p>Para acceder a nuestro sistema debes <a href="http://127.0.0.1:8000/">iniciar sesión</a>.</p>
                    <hr>
                    <p class="mb-0">Ramenezco App.</p>
                </div>
            </div>
            @include('nav.footer-nav') </div>
    </div>
</div>
</body>
</html>
@else
//




<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
    </div>


<div class="app-main">
@include('nav.side-bar')
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                            <div>Lista de Empleados
                                <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


<h1>{{ $modo }} Horario</h1>



<div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Empleados</h5>

@if(count($errors)>0)
    
    <div class="alert alert-danger" role="alert">
<ul>
     @foreach( $errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
   
@endif


<div class="form-group">
<label for="id_employee">Cédula </label>
<input type="text"  class="form-control" name="id_employee" value="{{ isset($schedule->id_employee)?$schedule->id_employee:old('id_employee') }}" id="id_employee">
<br>
</div>


<div class="form-group">
<label for="date">Fecha </label>
<select class="form-control" id="date" name="date" value="{{ isset($schedule->date)?$schedule->date:old('date') }}">
<option value="Lunes" @if (old('date') == "Lunes") selected @endif>Lunes</option>
<option value="Martes" @if (old('date') == "Martes") selected @endif>Martes</option>
<option value="Miercoles" @if (old('date') == "Miércoles") selected @endif>Miércoles</option>
<option value="Jueves" @if (old('date') == "Jueves") selected @endif>Jueves</option>
<option value="Viernes" @if (old('date') == "Viernes") selected @endif>Viernes</option>
<option value="Sabado" @if (old('date') == "Sábado") selected @endif>Sábado</option>
<option value="Domingo" @if (old('date') == "Domingo") selected @endif>Domingo</option>
</select>
<br>
</div>


<div class="form-group">
<label for="entrance_time">Hora de Entrada </label>
<input type="time"  class="form-control" name="entrance_time" value="{{ isset($schedule->entrance_time)?$schedule->entrance_time:old('entrance_time') }}" id="entrance_time">
<br>
</div>


<div class="form-group">
<label for="departure_time">Hora de Salida </label>
<input type="time"  class="form-control" name="departure_time" value="{{ isset($schedule->departure_time)?$schedule->departure_time:old('departure_time') }}" id="departure_time">
<br>
</div>


<div class="form-group">
<label for="extra_hours">Horas Extra </label>
<input type="text"  class="form-control" name="extra_hours" value="{{ isset($schedule->extra_hours)?$schedule->extra_hours:old('extra_hours') }}" id="extra_hours" >
<br>
</div>


<input class="btn btn-success" type="submit" value="{{$modo}} datos" >



<a href="{{ url('schedule/') }}"  class="btn btn-primary"> Regresar </a>

<br>


</div>  
                         
                </div>
            </div>
           @include('nav.footer-nav')
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>



//
@endif
