<div class="app-wrapper-footer">
    <div class="app-footer">
        <div class="app-footer__inner">
            <div class="app-footer-left">
                <ul class="nav">
                    <li class="nav-item"> <a href="javascript:void(0);" class="nav-link">&copy; Ramenezco, Todos los derechos reservados 2021</a> </li>
                </ul>
            </div>
            <div class="app-footer-right">
                <ul class="nav">
                    <li class="nav-item"> <a href="https://www.instagram.com/ramenezco.lab/" class="nav-link">Instagram </a> </li>
                    <li class="nav-item"> <a href="https://www.facebook.com/ramenezco.lab/" class="nav-link">Facebook </a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
