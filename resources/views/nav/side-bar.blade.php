<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span> </button>
        </div>
    </div>
    <div class="app-header__menu"> <span>
        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"> <span class="btn-icon-wrapper"> <i class="fa fa-ellipsis-v fa-w-6"></i> </span> </button>
        </span> </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">General</li>
                <li> <a href="notificationsAdmi" > <i class="metismenu-icon pe-7s-rocket"></i> Notificaciones </a> </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond" class="mm-active"></i> Calendario <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="evento3"> <i class="metismenu-icon"> </i>Ver Calendario </a> </li>
                        
                    </ul>
                </li>
                <li class="app-sidebar__heading">Administrar</li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Productos <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="products"> <i class="metismenu-icon"> </i>Listar, Editar y Borrar Productos </a> </li>
                        <li> <a href="products/create"> <i class="metismenu-icon"></i> Crear Productos </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Proveedores <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="suppliers"> <i class="metismenu-icon"> </i>Listar, Editar y Borrar Proveedores </a> </li>
                        <li> <a href="suppliers/create"> <i class="metismenu-icon"></i> Crear Proveedores </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Facturas <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="purchases"> <i class="metismenu-icon"> </i>Listar, Editar y Borrar Facturas </a> </li>
                        <li> <a href="purchases/create"> <i class="metismenu-icon"></i> Crear Facturas </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Inventarios de Productos <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="balances"> <i class="metismenu-icon"> </i>Listar, Editar y Borrar Inventarios de Productos </a> </li>
                        <li> <a href="balances/create"> <i class="metismenu-icon"></i> Crear Inventarios </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Entradas de Productos <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="movements"> <i class="metismenu-icon"> </i>Listar, Editar y Borrar Entradas de Productos </a> </li>
                        <li> <a href="movements/create"> <i class="metismenu-icon"></i> Crear Entradas de Productos </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Empleados <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="employee"> <i class="metismenu-icon"></i> Listar Empleados </a> </li>
                        <li> <a href="employee/create"> <i class="metismenu-icon"></i> Crear Nuevo Empleado </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Clientes <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="client"> <i class="metismenu-icon"> </i>Listar Clientes </a> </li>
                       <!-- <li> <a href="/client/create"> <i class="metismenu-icon"> </i>Agregar Cliente </a> </li>-->
                        <li> <a href="client"> <i class="metismenu-icon"> </i>Modificar Cliente </a> </li>
                        <li> <a href="client"> <i class="metismenu-icon"> </i>Eliminar Cliente </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Puntos Cliente Frecuente <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="define-percentage.php"> <i class="metismenu-icon"> </i>Porcentaje de Acumulación </a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Reportes <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                        <li> <a href="gastos"> <i class="metismenu-icon"> </i>Mostrar Reporte de Gasto por Semana de Productos </a> </li>
                        <li> <a href="dates"> <i class="metismenu-icon"> </i>Mostrar Reporte de Dias de Entrega de Productos </a> </li>
                        <li> <a href="requests"> <i class="metismenu-icon"> </i>Mostrar Reporte de Inventario, Gasto Semanl y Dias de Entrega por Producto</a> </li>
                    </ul>
                </li>
                <li> <a href="#"> <i class="metismenu-icon pe-7s-diamond"></i> Usuarios <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i> </a>
                    <ul>
                    <li> <a href="role"> <i class="metismenu-icon"> </i>Listar Usuarios </a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
