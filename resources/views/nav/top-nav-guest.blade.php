
<div class="app-header header-shadow">
<div class="app-header__logo">
    <div class="logo-src"></div>
    
</div>

<div class="app-header__content">
    
    <div class="app-header-right">
        <div class="header-btn-lg pr-0">
            <div class="widget-content p-0">
                <div class="widget-content-wrapper">
                @guest
                    <div class="widget-content-left  ml-3 header-user-info">
                        
                        
                    
                            @if (Route::has('login'))

                                <div class="widget-heading"> <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> </div> 
                            
                            @endif

                            @if (Route::has('register'))
                                
                                <div class="widget-subheading"> <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a> </div>  
                            
                            @endif
                            </div>
                        @else
                    <div class="widget-content-left">
                        <div class="btn-group"> <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn"> <i class="pe-7s-users" style="font-size: 1.85rem;"> </i> 
                            <!--<img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">--> 
                            <i class="fa fa-angle-down ml-2 opacity-8"></i> </a>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content-left  ml-3 header-user-info">
                        <div class="widget-heading"> {{ Auth::user()->name }} </div>
                        <div class="widget-subheading"> Visita </div>
                    </div>
                    <div class="widget-content-right header-user-info ml-3">
                        <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example"> <i class="fa text-white fa-calendar pr-1 pl-1"></i> </button>
                    </div>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>

