<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Producto') }}
            {{ Form::text('Producto', $request->Producto, ['class' => 'form-control' . ($errors->has('Producto') ? ' is-invalid' : ''), 'placeholder' => 'Producto']) }}
            {!! $errors->first('Producto', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Inventario Actual') }}
            {{ Form::text('Inventario', $request->Inventario, ['class' => 'form-control' . ($errors->has('Inventario') ? ' is-invalid' : ''), 'placeholder' => 'Inventario Actual']) }}
            {!! $errors->first('Inventario', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Gasto Promedio Semanal') }}
            {{ Form::text('GastoPromedioSemanal', $request->GastoPromedioSemanal, ['class' => 'form-control' . ($errors->has('GastoPromedioSemanal') ? ' is-invalid' : ''), 'placeholder' => 'Gasto Promedio Semanal']) }}
            {!! $errors->first('GastoPromedioSemanal', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('DiasEntrega') }}
            {{ Form::text('DiasEntrega', $request->DiasEntrega, ['class' => 'form-control' . ($errors->has('DiasEntrega') ? ' is-invalid' : ''), 'placeholder' => 'DiasEntrega']) }}
            {!! $errors->first('DiasEntrega', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>