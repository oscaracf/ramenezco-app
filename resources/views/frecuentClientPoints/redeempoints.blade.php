<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Deducir Puntos de Cliente Frecuente - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    @include('nav.top-nav')
</div>
<div class="app-main">
@include('nav.side-bar')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="lnr-picture text-danger"> </i> </div>
                        <div>Cangear Puntos
                            <div class="page-title-subheading"> <!-- Inline validation is very easy to implement using the Architect Framework.--> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Reducción de Puntos de Cliente:</h5>
                    <form action="{{ url('/frecuentClientPoints') }}" method="post" class="needs-validation" novalidate>
                    @csrf
                
                    {{  isset($frecuentClientPoints)?$frecuentClientPoints:"" }}

                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom01">Identificación:</label>
                            <input type="text" class="form-control" id="validationCustom06" placeholder="Identificación:" value="{{  isset($user[0]->id)?$user[0]->id:'' }}" name="Id_user" readonly>
                            <div class="invalid-feedback"> Formato Inválido. Nacional: XX-XXXX-XXXX, DIMEX: XXXXXXXXXXX ó XXXXXXXXXXXX (11 o 12 dígitos, sin cero al inicio ni guiones), Pasaporte: XXXXXXXXXXXXXXXXXXXX (20 dígitos maximo, pueden ser números o letras).<br>
                                Información Obligatoria. </div>
                            <div class="valid-feedback"> Formato Verificado. </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom02">Puntos Disponibles</label>
                            <input type="number" class="form-control" id="validationCustom02" placeholder="1" aria-describedby="inputGroupPrepend" name="total_points" value="{{  @$totalpoints }}" readonly>
                            <div class="invalid-feedback"> Formato Inválido.<br>
                            Información Obligatoria. </div>
                             <div class="valid-feedback"> Formato Verificado. </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationCustom02">Tipo de Client Frecuente</label>
                            <input type="number" class="form-control" id="validationCustom02" placeholder="1" aria-describedby="inputGroupPrepend" name="id_points" value="{{  isset($client[0]->id_points)?$client[0]->id_points:''}}" readonly>
                            <div class="invalid-feedback"> Formato Inválido.<br>
                            Información Obligatoria. </div>
                             <div class="valid-feedback"> Formato Verificado. </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom01">Monto a Cangear:</label>
                            <input type="number" class="form-control" id="validationCustom03" placeholder="0" value="{{  isset($frecuentClientPoints[0]->points_earned)?$frecuentClientPoints[0]->points_earned:'' }}" name="points_earned">
                            <div class="invalid-feedback"> Formato Inválido. Esta conformada por un número entero, de máximo siete dígitos: XXXXXXX..<br>
                                Información Obligatoria. </div>
                            <div class="valid-feedback"> Formato Verificado. </div>
                        </div>
                        <div class="col-md-3 mb-3" style="display:none">
                            <input type="text" class="form-control" id="" placeholder="0" value="redeem" name="transaction_type">
                        </div>
                    </div>
                    
                    <button class="btn btn-primary" type="submit" >Cangear Puntos</button>



               
                        
                        
                    </form>
                    <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    /*if (form.checkValidity() === true) {
                                                        event.preventDefault();
                                                        console.log("No ignorado");
                                                        $('#exampleModal').modal('show');
                                                    } */
                                                    console.log(form.checkValidity());
                                                    form.classList.add('was-validated');  
                                                }, false);
                                            });
                                            $('#purchaseAmount').change(function (e) { 
                                                e.preventDefault();
                                               
                                                var aux = 0.02 * $('#purchaseAmount').val();
                                                console.log(aux);
                                                $('#pointsEarned').val(aux);
                                                
                                               
                                                console.log("si entra");
                                                
                                                
                                            });
                                            
                                        }, false);
                                    })();
                                </script> 
                </div>
            </div>
        </div>
        @include('nav.footer-nav')
    </div>
</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Resultado de Creación de Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cuenta ha sido creada satisfactoriamente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">Continuar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>






