
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Listar Historial de Puntos - Ramenezco</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description" content="Inline validation is very easy to implement using the Architect Framework.">
<meta name="msapplication-tap-highlight" content="no">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>


@if (Auth::guest())
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
@include('nav.top-nav-guest')


</div>
<div class="app-main">

    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Historial 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            
            <div>
            @include('validation.login')
</div>
            <!--<div class="row">
                <div class="col-lg-12">
                    <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>-->
            @include('nav.footer-nav')
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>

</body>
</html>


@else
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
@include('nav.top-nav-index')
</div>
<div class="app-main">

@include('nav.side-bar-index')

    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon"> <i class="pe-7s-drawer icon-gradient bg-happy-itmeo"> </i> </div>
                        <div>Listar Historial de Puntos 
                            <!--<div class="page-title-subheading">Tables are the backbone of almost all web applications.
                                        </div>--> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Historial</h5>
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Identificación</th>
                                            <th>Tipo de Cliente</th>
                                            <th>Monto de Compra</th>
                                            <th>Puntos Acumulados</th>
                                            <th>Tipo de Acumulación</th>
                                            <th>Fecha de Acumulación</th>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <span style="display:none;">{{ $aux = 1  }}
                                    {{$total_points = 0}}</span>
                                        
                                    @foreach( $records as $record )
                                       
                                                <tr id="">
                                                    <td scope="row">
                                                        {{ $aux++ }}
                                                    </td>
                                                    <td>{{ $record->id_user }}</td>
                                                    <td>
                                                    @if ( $record->client_type == 1)
                                                        Oro
                                                    @elseif ($record->client_type == 2)
                                                        Plata
                                                    @else
                                                        Bronce
                                                    @endif                                                         
                                                    </td>
                                                    <td>{{ $record->purchase_amount }}</td>
                                                    <td>{{ $record->points_earned}} <span style="display:none;">{{$total_points = $total_points+ $record->points_earned}}</span></td>
                                                    <td>
                                                    @if ( $record->transaction_type == "earn")
                                                        Acumulación
                                                    @else
                                                        Canje
                                                    @endif                                                         
                                                    </td>


                                                    <td>{{ $record->created_at }}</td>

                                                    </tr>
                                                    @endforeach
                                               
                                                
                                    </tbody>
                                </table>
                                <table class="mb-0 table col-lg-6" style="margin: 40px 0 0 0; font-size: 18px;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Identificación</th>
                                            <th>Total Puntos Displonibles</th>
                                           
                                         
                                        </tr>
                                        </thead>
                                    <tbody>
                                        <tr id="">
                                            <td scope="row">
                                                {{ $aux++ }}
                                            </td>
                                            <td>{{ $record->id_user }}</td>
                                            <td>{{ $total_points }}</td>
                                           
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!--<div class="row">
                <div class="col-lg-12">
                    <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>-->
            @include('nav.footer-nav')
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">¿Esta seguro que desea eliminar esta cuenta?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="removeRow(row);">Aceptar</button>
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>--> 
            </div>
        </div>
    </div>
</div>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    /*var row = 0;
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            $('.btn-danger').click(function (e) { 
                e.preventDefault();
                var button = $(this).attr('id');
                switch(button) {
                    case "delete-button1":
                        row = 1;
                    break;
                    case "delete-button2":
                        row = 2;
                    break;
                    case "delete-button3":
                        row = 3;
                    break;
                    case "delete-button4":
                        row = 4;
                    break;
                }
                $('#exampleModal').modal('show');
                console.log("si entra");
                
            });
            $('.dropdown-item').click(function (e) { 
                //$('.dropdown-menu').show();
                e.preventDefault();
                var btnAux = $(this).attr('id');
                switch(btnAux) {
                    case "btnAcu":
                        window.location.href='accumulate-points.php'
                    break;
                    case "btnRed":
                        window.location.href='redeem-points.php'
                    break;
                }
                
                console.log("si entra");
                
            });
        }, false);
    })();
    function removeRow (aux){
        console.log("si");
        
        $('#row'+aux).remove();
        $('#exampleModal').modal('hide');
        
    }*/
</script> 
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
@endif
