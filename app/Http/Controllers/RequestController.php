<?php

namespace App\Http\Controllers;

use App\Models\Request;
/* use Illuminate\Http\Request; */
use Illuminate\Support\Facades\DB;

/**
 * Class DateController
 * @package App\Http\Controllers
 */
class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
        //$clear1 = DB::select("call deleteTablaRequests()");
        $clear = DB::table('requests')->delete();
        $clear3 = DB::select("call Pedidos()");
        $requests= Request::paginate();

        return view('request.index', compact('requests'))
            ->with('i', (request()->input('page', 1) - 1) * $requests->perPage());
    }

 }
