<?php

namespace App\Http\Controllers;

use App\Models\Evento2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Evento2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('evento2.index');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate(Evento2::$rules);
        $evento2=Evento2::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Evento2  $evento2
     * @return \Illuminate\Http\Response
     */
    public function show(Evento2 $evento2)
    {
        //
        $evento2=Evento2::all();
        return response()->json($evento2);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Evento2  $evento2
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento2 $evento2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Evento2  $evento2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evento2 $evento2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Evento2  $evento2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento2 $evento2)
    {
        //
    }
}
