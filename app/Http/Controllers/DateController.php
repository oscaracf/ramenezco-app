<?php

namespace App\Http\Controllers;

use App\Models\Date;
/*use Illuminate\Http\Request;*/
use Illuminate\Support\Facades\DB;

/**
 * Class DateController
 * @package App\Http\Controllers
 */
class DateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
        //$clear1 = DB::select("call deleteTablaDates()");

        $clear = DB::table('dates')->delete();
        $clear3 = DB::select("call DiasEntrega()");
        $dates = Date::paginate();

        return view('date.index', compact('dates'))
            ->with('i', (request()->input('page', 1) - 1) * $dates->perPage());
    }

 }
