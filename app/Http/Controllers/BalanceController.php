<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use Illuminate\Http\Request;

/**
 * Class BalanceController
 * @package App\Http\Controllers
 */
class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $balances = Balance::paginate();

        return view('balance.index', compact('balances'))
            ->with('i', (request()->input('page', 1) - 1) * $balances->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $balance = new Balance();
        return view('balance.create', compact('balance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Balance::$rules);

        $balance = Balance::create($request->all());

        return redirect()->route('balances.index')
            ->with('success', 'Balance created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $balance = Balance::find($id);

        return view('balance.show', compact('balance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $balance = Balance::find($id);

        return view('balance.edit', compact('balance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Balance $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balance $balance)
    {
        request()->validate(Balance::$rules);

        $balance->update($request->all());

        return redirect()->route('balances.index')
            ->with('success', 'Balance updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $balance = Balance::find($id)->delete();

        return redirect()->route('balances.index')
            ->with('success', 'Balance deleted successfully');
    }
}
