<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $texto=trim($request->get('texto'));
        $semana=trim($request->get('semana'));
        $request = ['texto' => $texto, 'semana' => $semana];

        $schedules=DB::table('schedules')
            ->select('id','id_employee', 'date','entrance_time','departure_time', 'extra_hours')
            ->where('id_employee', 'LIKE', '%'.$texto.'%')
            ->orWhere('date', 'LIKE', '%'.$texto.'%')
            ->orderBy('date','asc')
            ->paginate(10);
        return view('schedule.index',compact('schedules','texto', 'semana'));
 
       
        //$datos['schedules']=Schedule::paginate(10);
        //return view('schedule.index',$datos);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('schedule.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campo=[
            'id_employee'=>'required|integer',
            'entrance_time'=>'required',
            'departure_time'=>'required',
            'extra_hours'=>'required|integer'
        ];


        $mensaje=[
            'id_employee.required' => 'La cedula es requerida',      
            'entrance_time.required' => 'La hora de entrada es requerida' ,      
            'departure_time.required' => 'La hora de salida es requerida',       
            'extra_hours.required' => 'Las horas extra son requeridas (en caso que no tenga horas extras ingrese un 0)'       

        
        ];


        $this->validate($request,$campo, $mensaje);

        $datosSchedule = request()->except('_token');
        Schedule::insert($datosSchedule);

        return redirect('schedule')->with('mensaje', 'Horario agregado con exito');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $schedule = Schedule::where('id_employee', $id)->get();

        //$schedule=Schedule::findOrFail($id);
        return view('schedule.edit', compact('schedule'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campo=[
            'id_employee'=>'required|integer',
            'entrance_time'=>'required',
            'departure_time'=>'required',
            'extra_hours'=>'required|integer'
        ];


        $mensaje=[
            'id_employee.required' => 'La cedula es requerida',        
            'entrance_time.required' => 'La hora de entrada es requerida' ,      
            'departure_time.required' => 'La hora de salida es requerida',       
            'extra_hours.required' => 'Las horas extra son requeridas (en caso que no tenga horas extras ingrese un 0)'       

        
        ];


        $this->validate($request,$campo, $mensaje);


        $datosSchedule = request()->except('_token','_method');
        Schedule::where('id_employee', '=', $id)->update($datosSchedule);
        
        $schedule = Schedule::where('id_employee', $id)->get();

        //$schedule=Schedule::findOrFail($id);
        return redirect('schedule')->with('mensaje','Horario Modificado');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Schedule::where('id', $id)->delete();
        return redirect('schedule')->with('mensaje','Horario Borrado');    
    }
}
