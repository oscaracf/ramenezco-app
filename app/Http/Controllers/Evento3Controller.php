<?php

namespace App\Http\Controllers;

use App\Models\Evento3;
use Illuminate\Http\Request;

class Evento3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('evento3.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate(Evento3::$rules);
        $evento3=Evento3::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Evento3  $evento3
     * @return \Illuminate\Http\Response
     */
    public function show(Evento3 $evento3)
    {
        //
        $evento3=Evento3::all();
        return response()->json($evento3);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Evento3  $evento3
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $evento3=Evento3::find($id);
        return response()->json($evento3);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Evento3  $evento3
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Evento3 $evento3)
    {
        //
        request()->validate(Evento3::$rules);
        $evento3->update($request->all());
        return response()->json($evento3);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Evento3  $evento3
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $evento3=Evento3::find($id)->delete();
        return response()->json($evento3);
    }
}
