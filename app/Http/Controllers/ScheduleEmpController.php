<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Models\scheduleEmp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ScheduleEmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $texto=trim($request->get('texto'));

        $schedules=DB::table('schedules')
            ->select('id','id_employee', 'date','entrance_time','departure_time', 'extra_hours')
            ->where('id_employee', 'LIKE', '%'.$texto.'%')
            
            ->orderBy('date','asc')
            ->paginate(10);

        return view('scheduleEmp.index',compact('schedules','texto'));


        //$scheduleEmp = Schedule::all();
        //return view('scheduleEmp.index')->with('schedules',$scheduleEmp);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\scheduleEmp  $scheduleEmp
     * @return \Illuminate\Http\Response
     */
    public function show(scheduleEmp $scheduleEmp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\scheduleEmp  $scheduleEmp
     * @return \Illuminate\Http\Response
     */
    public function edit(scheduleEmp $scheduleEmp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\scheduleEmp  $scheduleEmp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, scheduleEmp $scheduleEmp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\scheduleEmp  $scheduleEmp
     * @return \Illuminate\Http\Response
     */
    public function destroy(scheduleEmp $scheduleEmp)
    {
        //
    }
}
