<?php

namespace App\Http\Controllers;

use App\Models\NotificationsEmp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationsEmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $notificationEmp= NotificationsEmp::all();
        return view('notificationEmp.notificationEmp', array('notificationEmp'=>$notificationEmp));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NotificationsEmp  $notificationsEmp
     * @return \Illuminate\Http\Response
     */
    public function show(NotificationsEmp $notificationsEmp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NotificationsEmp  $notificationsEmp
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationsEmp $notificationsEmp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NotificationsEmp  $notificationsEmp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificationsEmp $notificationsEmp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NotificationsEmp  $notificationsEmp
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotificationsEmp $notificationsEmp)
    {
        //
    }
}
