<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\Models\NotificationsAdmi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;

class NotificationsAdmiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $notifications = Notifications::all();
        return view('notificationAdmi.notification')->with('notifications',$notifications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NotificationsAdmi  $notificationsAdmi
     * @return \Illuminate\Http\Response
     */
    public function show(NotificationsAdmi $notificationsAdmi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NotificationsAdmi  $notificationsAdmi
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationsAdmi $notificationsAdmi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NotificationsAdmi  $notificationsAdmi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificationsAdmi $notificationsAdmi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NotificationsAdmi  $notificationsAdmi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Notifications::where('id',$id)->delete();
        return redirect('notificationsAdmi')->with('mensaje','Empleado Borrado');
    }
}
