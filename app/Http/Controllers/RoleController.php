<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['users'] = User::all();
       //echo $user;
       return view('role.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //DB::enableQueryLog(); 

        //$client = Client::where('id_client', $id);
       // $client = Client::where('id_client', $id)->findOrFail();
       $user = User::where('id','=',$id)->get();
        //$client = Client::where('id_client','=',$id)->get();
        //dd(DB::getQueryLog()); 
       return view('role.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //
         $datosEmpleado = request()->except('_token','_method');

         $userType = $datosEmpleado['Role'];
         //print_r ($datosEmpleado);
         User::where('id', $id)->update($datosEmpleado);
         $user = User::where('id','=',$id)->get();

         if($userType == 3)
            return view('client.create', compact('user'))->with('message','Role de usuario ha sido modificado a cliente satisfactoriamente.'); 
        elseif($userType == 2)
            return view('empleado.create', compact('user'))->with('message','Role de usuario ha sido modificado a empleado satisfactoriamente.');
        else
            return redirect('role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
