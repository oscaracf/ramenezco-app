<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['employees']=Employee::paginate(18);
        return view('employee.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
        //return view('employee.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //validar datos
        $campo=[
            'id_employee'=>'required|integer',
            'name'=>'required|string',
            'first_last_name'=>'required|string',
            'second_last_name'=>'required|string',
            'allergies'=>'required|string',
            'province'=>'required|string',
            'canton'=>'required|string',
            'district'=>'required|string',
            'adress'=>'required|string',
            'email'=>'required|email',
            'date_entrance'=>'required|date',
            'phone_number'=>'required|integer',
            'occupation'=>'required|string',
            'current_salary'=>'required|integer',
        ];

        
        //mensajes para mostrar al usuario del error
        $mensaje=[
            'id_employee.required' => 'Es necesario llenar Cédula' ,
            'id_employee.integer' => 'Unicamente números en Cédula' ,

            'name.required' => 'Es necesario llenar Nombre' ,
            'name.string' => 'Unicamente letras en Nombre' ,

            'first_last_name.required' => 'Es necesario llenar Primer Apellido' ,
            'first_last_name.string' => 'Unicamente letras en Primer Apellido' ,

            'second_last_name.required' => 'Es necesario llenar Segundo Apellido' ,
            'second_last_name.string' => 'Unicamente letras en Segundo Apellido' ,

            'allergies.required' => 'Es necesario llenar Alergias ',
            'allergies.string' => 'Unicamente letras en Alergias',

            'province.required' => 'Es necesario llenar Provincia' ,
            'province.string' => 'Unicamente letras en Provincia' ,

            'canton.required' => 'Es necesario llenar Cantón' ,
            'canton.string' => 'Unicamente letras en Cantón' ,

            'district.required' => 'Es necesario llenar Distrito' ,
            'district.string' => 'Unicamente letras en Distrito' ,

            'adress.required' => 'Es necesario llenar Dirección' ,

            'email.required' => 'Es necesario llenar Correo Electrónico' ,

            'date_entrance.required' => 'Es necesario llenar Día de entrada' ,

            'phone_number.required' => 'Es necesario llenar Número telefónico',
            'phone_number.integer' => 'Unicamente números en Número telefónico',

            'occupation.required' => 'Es necesario llenar Ocupación' ,
            'occupation.string' => 'Unicamente letras en Ocupación' ,

            'current_salary.required' => 'Es necesario llenar Salario' ,
            'current_salary.integer' => 'Unicamente números en Salario' ,
        ];
   

        $this->validate($request,$campo, $mensaje);


        $datosEmployee = request()->except('_token');
        
        //echo $random_password;

        Employee::insert($datosEmployee); 
        //return response()->json($datosEmployee);
        return redirect('employee')->with('mensaje', 'Empleado agregado con exito');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = Employee::where('id_employee', $id)->get();
        return view('employee.edit', compact('employee') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

       //validar datos
      /* $campo=[
        'id_employee'=>'required|integer',
        'name'=>'required|string',
        'first_last_name'=>'required|string',
        'second_last_name'=>'required|string',
        'allergies'=>'required|string',
        'province'=>'required|string',
        'canton'=>'required|string',
        'district'=>'required|string',
        'adress'=>'required|string',
        'email'=>'required|email',
        'date_entrance'=>'required|date',
        'phone_number'=>'required|integer',
        'occupation'=>'required|string',
        'current_salary'=>'required|integer',
        'schedule'=>'required',
    ];

    
    //mensajes para mostrar al usuario del error
    $mensaje=[
        'id_employee.required' => 'Es necesario llenar Cédula' ,
        'id_employee.integer' => 'Unicamente números en Cédula' ,

        'name.required' => 'Es necesario llenar Nombre' ,
        'name.string' => 'Unicamente letras en Nombre' ,

        'first_last_name.required' => 'Es necesario llenar Primer Apellido' ,
        'first_last_name.string' => 'Unicamente letras en Primer Apellido' ,

        'second_last_name.required' => 'Es necesario llenar Segundo Apellido' ,
        'second_last_name.string' => 'Unicamente letras en Segundo Apellido' ,

        'allergies.required' => 'Es necesario llenar Alergias ',
        'allergies.string' => 'Unicamente letras en Alergias',

        'province.required' => 'Es necesario llenar Provincia' ,
        'province.string' => 'Unicamente letras en Provincia' ,

        'canton.required' => 'Es necesario llenar Cantón' ,
        'canton.string' => 'Unicamente letras en Cantón' ,

        'district.required' => 'Es necesario llenar Distrito' ,
        'district.string' => 'Unicamente letras en Distrito' ,

        'adress.required' => 'Es necesario llenar Dirección' ,

        'email.required' => 'Es necesario llenar Correo Electrónico' ,

        'date_entrance.required' => 'Es necesario llenar Día de entrada' ,

        'phone_number.required' => 'Es necesario llenar Número telefónico',
        'phone_number.integer' => 'Unicamente números en Número telefónico',

        'occupation.required' => 'Es necesario llenar Ocupación' ,
        'occupation.string' => 'Unicamente letras en Ocupación' ,

        'current_salary.required' => 'Es necesario llenar Salario' ,
        'current_salary.integer' => 'Unicamente números en Salario' ,

        'schedule.required' => 'Es necesario llenar Horario' 

    ];

        $this->validate($request,$campo, $mensaje);*/


        
        $datosEmployee = request()->except('_token','_method');

      

        Employee::where('id_employee','=',$id)->update($datosEmployee);
        $employee = Employee::where('id_employee', $id)->get();
        //return view('employee.edit', compact('employee') );
        return redirect('employee')->with('mensaje','Empleado Modificado');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Employee::where('id_employee', $id)->delete();
        return redirect('employee')->with('mensaje','Empleado Borrado');
    }
}
