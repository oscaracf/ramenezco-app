<?php

namespace App\Http\Controllers;

use App\Models\FrecuentClientPoints;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FrecuentClientPointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pointsHistory['records']=FrecuentClientPoints::paginate(5000);
         return view('frecuentClientPoints.index', $pointsHistory);
        // return view('FrecuentClientPoints.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datosEmpleado = request()->all();
        $dataFrecuentClient = request()->except('_token', 'total_points');
        $current_date_time = Carbon::now()->toDateTimeString(); 

        $dataFrecuentClient['Created_At'] = $current_date_time;
        $id=$dataFrecuentClient['Id_user'];

        if($dataFrecuentClient['transaction_type'] !="earn")
            $dataFrecuentClient['points_earned'] = -$dataFrecuentClient['points_earned'];


        //$id_points=$dataFrecuentClient['id_points'];
        FrecuentClientPoints::insert($dataFrecuentClient);

        
        //redirect('client'); 
        //$datos['pointsHistory'] = FrecuentClientPoints::where('id','=',$dataFrecuentClient['id'])->get();

        $pointsHistory['records']=FrecuentClientPoints::where('id_user', '=', $id)->paginate(2000);

       /* $pointsHistory['records'] = DB::table('frecuent_client_points')
                ->select('id','client_type','purchase_amount','points_earned','created_at')
                ->where('id','=',$id)
                ->get();*/

              //  SELECT id, SUM(points_earned) as 'total_points' FROM `frecuent_client_points` WHERE 1
        //echo $pointsHistory;
        

       //return $dataFrecuentClient['transaction_type'];
       return view('frecuentClientPoints.list', $pointsHistory);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function show(FrecuentClientPoints $frecuentClientPoints)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id','=',$id)->get();
        $client = Client::where('id_client','=',$id)->get();
       //echo $user;
      
        return view('frecuentClientPoints.earnpoints', compact('user', 'client'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function redeem($id)
    {
        $user = User::where('id','=',$id)->get();
        $client = Client::where('id_client','=',$id)->get();
        $totalpoints = FrecuentClientPoints::where('id_user','=',$id)->sum('points_earned');
    
        
       //echo $user;
      //return $totalpoints;
        return view('frecuentClientPoints.redeempoints', compact('user', 'totalpoints', 'client'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function history_user($id)
    {
        $pointsHistory['records']=FrecuentClientPoints::where('id_user', '=', $id)->paginate(2000);
       return view('frecuentClientPoints.list', $pointsHistory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FrecuentClientPoints $frecuentClientPoints)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FrecuentClientPoints  $frecuentClientPoints
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrecuentClientPoints $frecuentClientPoints)
    {
        //
    }

    
}
