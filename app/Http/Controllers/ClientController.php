<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use Carbon\Carbon;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['clients'] = Client::paginate(5000);
        return view('client.index', $datos);
        //return print_r($datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     *  @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        //$datosEmpleado = request()->all();
        //$random_password = Str::random(8);
        //$hashed_random_password = Hash::make($random_password);
        //Temporal para ver el password $datosEmpleado['Password'] = $hashed_random_password;
        //$datosEmpleado['Password'] = $random_password;

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientData = request()->except('_token');
        $current_date_time = Carbon::now()->toDateTimeString(); 

        $clientData['Created_At'] = $current_date_time;
        $clientData['Id_Points'] = 1;
        Client::insert($clientData);
        return redirect('client')->with('message','Cliente ha sido creado satisfactoriamente.'); 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //DB::enableQueryLog(); 

        //$client = Client::where('id_client', $id);
       // $client = Client::where('id_client', $id)->findOrFail();
       //$client = Client::where('id_client','=',$id)->get();
       // $client=Client::select('*')->where('id_client',$id)->get();

        //dd(DB::getQueryLog()); 
       //return view('client.edit', compact('client'));

      /* $client = DB::table('clients')
       ->select('*')
       ->where('Id_client', $id)
       ->get();*/
       $client = Client::where('id_client', $id)->get();

       //echo $user;
      
        return view('client.edit', compact('client'));

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosEmpleado = request()->except('_token','_method');
        //print_r ($datosEmpleado);
        Client::where('id_client', $id)->update($datosEmpleado);
        $client = Client::where('id_client','=',$id)->get();
        return redirect('client')->with('message','Cliente ha sido modificado satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
        //echo $id;
        Client::where('id_client', $id)->delete();
        return redirect('client')->with('message','Cliente ha sido eleminado satisfactoriamente.');
    }

    public function sendEmail($random_password)
    {
        //
        return view('client.create');
    }
}
