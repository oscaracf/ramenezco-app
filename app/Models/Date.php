<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Date
 *
 * @property $Producto
 * @property $DiasEntrega
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Date extends Model
{
    
    static $rules = [
		'Producto' => 'required',
		'DiasEntrega' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Producto','DiasEntrega'];  

}
