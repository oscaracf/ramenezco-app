<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Balance
 *
 * @property $id
 * @property $Codigo
 * @property $FechaMovimiento
 * @property $SaldoActual
 * @property $created_at
 * @property $updated_at
 * @property $product_id
 *
 * @property Supplier $supplier
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Balance extends Model
{
    
    static $rules = [
    'Codigo' => 'required',
    'FechaMovimiento' => 'required',
		'SaldoActual' => 'required',
		'product_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Codigo','FechaMovimiento','SaldoActual','product_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    

}
