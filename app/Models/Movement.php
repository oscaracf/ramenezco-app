<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Movement
 *
 * @property $id
 * @property $Codigo
 * @property $FechaMovimiento
 * @property $cantidad
 * @property $created_at
 * @property $updated_at
 * @property $purchase_id
 * @property $product_id
 *
 * @property Purchase $purchase
 * @property Product $product

 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Movement extends Model
{
    
    static $rules = [
		'Codigo' => 'required',
        'FechaMovimiento' => 'required',
		'cantidad' => 'required',
		'purchase_id' => 'required',
		'product_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Codigo','FechaMovimiento','cantidad','purchase_id','product_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function purchase()
    {
        return $this->hasOne('App\Models\Purchase', 'id', 'purchase_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    

}
