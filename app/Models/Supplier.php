<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Supplier
 *
 * @property $id
 * @property $Codigo
 * @property $Descripcion
 * @property $Telefono
 * @property $Provincia
 * @property $Canton
 * @property $Distrito
 * @property $Ubicacion
 * @property $Contacto
 * @property $created_at
 * @property $updated_at
 *
 * @property Purchase[] $purchases
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Supplier extends Model
{
    
    static $rules = [
		'Codigo' => 'required',
		'Descripcion' => 'required',
		'Telefono' => 'required',
		'Provincia' => 'required',
		'Canton' => 'required',
		'Distrito' => 'required',
		'Ubicacion' => 'required',
		'Contacto' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Codigo','Descripcion','Telefono','Provincia','Canton','Distrito','Ubicacion','Contacto'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchases()
    {
        return $this->hasMany('App\Models\Purchase', 'supplier_id', 'id');
    }
    

}
