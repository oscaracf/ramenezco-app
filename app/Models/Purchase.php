<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Purchase
 *
 * @property $id
 * @property $NumeroFactura
 * @property $FechaCompra
 * @property $cantidad
 * @property $valorUnitario
 * @property $created_at
 * @property $updated_at
 * @property $product_id
 * @property $supplier_id
 *
 * @property Product $product
 * @property Supplier $supplier
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Purchase extends Model
{
    
    static $rules = [
		'NumeroFactura' => 'required',
        'FechaCompra' => 'required',
		'cantidad' => 'required',
		'valorUnitario' => 'required',
		'product_id' => 'required',
		'supplier_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['NumeroFactura','FechaCompra','cantidad','valorUnitario','product_id','supplier_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function supplier()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }
    

}
