<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evento2 extends Model
{
    use HasFactory;

    static $rules=[
        'id_employee'=>'required',
        'name'=>'required',
        'occupation'=>'required',
        'start'=>'required',
        'end'=>'required'
    ];

    protected $fillable=['id_employee','name','occupation','start','end'];

}
