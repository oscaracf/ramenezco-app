<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Date
 *
 * @property $Producto
 * @property $Inventario
 * @property $GastoPromedioSemanal
 * @property $DiasEntrega
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Request extends Model
{
    
    static $rules = [
		'Producto' => 'required',
		'Inventario' => 'required',
		'GastoPromedioSemanal' => 'required',
		'DiasEntrega' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Producto','Inventario','GastoPromedioSemanal','DiasEntrega'];  

}
